<%-- 
    Document   : misPrestamos
    Created on : 30-06-2017, 14:18:16
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="text-left logotipo">
                <img src="./img/logo_duoc.png">
            </div>
            <div class="text-right nombre-sistema">
                <span>SISPA</span>
            </div>
        </header>
        <div class="main-container">
            <aside class="aside">
                <ul class="navigation-menu">
                    <li>
                        <a href="CatalogoProductos">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Catálogo
                        </a>
                    </li>
                    <li>
                        <a href="MisSolicitudes">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Mis Solicitudes
                        </a>
                    </li>
                    <li>
                        <a href="MisPrestamos" class="active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Mis Préstamos
                        </a>
                    </li>
                    <li>
                        <a href="MisDatos">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Mis Datos
                        </a>
                    </li>
                    <li>
                        <a href="Logout">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Salir
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="main-content">
                <div class="mensaje-bienvenida">
                    <c:set var="nombreUserLog" scope="session" value="${sessionScope.nombreUserLog}"></c:set>
                    Hola, <c:out value="${nombreUserLog}"></c:out>
                </div>
                <div class="main-heading">
                    <h1>Mis Préstamos</h1>
                </div>
                <div class="content">
                <% int count = 0;%>
                <c:forEach var="prestamo" items="${misPrestamos}">
                    <div class="panel">
                        <div class="panel-heading" role="tab" id="heading<%=count%>">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<%=count%>" aria-expanded="true" aria-controls="collapse<%=count%>">
                                <div>Código Solicitud: <c:out value="${prestamo.id}"></c:out></div>
                                <div>Fecha Solicitud: <fmt:formatDate type="date" value="${prestamo.fecha_creacion}"></fmt:formatDate></div>
                                <div class="text-right">Estado: <b><c:out value="${ listadoEstadoSolicitud.get( prestamo.estado_solicitud_id-1 ).descripcion }"></c:out></b></div>
                            </a>
                        </div>
                        <div id="collapse<%=count%>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<%=count%>">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">Código Producto</th>
                                        <th>Nombre Producto</th>
                                        <th class="text-center">Cantidad Solicitada</th>
                                        <th class="text-center">Cantidad Prestada</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="relacionSubcategoria" items="${prestamo.listadoProductos}">
                                    <tr>
                                        <td class="text-center"><c:out value="${relacionSubcategoria.subcategoria.id}"></c:out></td>
                                        <td><c:out value="${relacionSubcategoria.subcategoria.nombre}"></c:out></td>
                                        <td class="text-center"><c:out value="${relacionSubcategoria.cantidad_solicitud}"></c:out></td>
                                        <td class="text-center"><c:out value="${relacionSubcategoria.cantidad_prestada}"></c:out></td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <% count++; %>
                </c:forEach>
                </div>
            </section>
        </div>
    </body>
</html>