<%-- 
    Document   : panoleroListadoCategorias
    Created on : 21-06-2017, 22:52:58
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="text-left logotipo">
                <img src="./img/logo_duoc.png">
            </div>
            <div class="text-right nombre-sistema">
                <span>SISPA</span>
            </div>
        </header>
        <div class="main-container">
            <aside class="aside">
                <ul class="navigation-menu">
                    <li>
                        <a href="AdminListadoProductos">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Productos
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoCategorias" class="active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Categorías
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoMarcaProveedor">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Marca / Proveedor
                        </a>
                    </li>
                    <li>
                        <a href="Logout">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Salir
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="main-content">
                <div class="mensaje-bienvenida">
                    <c:set var="nombreUserLog" scope="session" value="${sessionScope.nombreUserLog}"></c:set>
                    Hola, <c:out value="${nombreUserLog}"></c:out>
                </div>
                <div class="main-heading">
                    <h1>Listado Categorías</h1>
                </div>
                <div class="content">
                    <a href="AdminAgregarCategoria" class="btn-yellow" style="margin-bottom: 20px; display: inline-block;">Agregar nueva categoría</a>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="categoria" items="${listadoCategorias}" >
                                <tr>
                                    <td><c:out value="${categoria.id}"></c:out></td>
                                    <td><c:out value="${categoria.nombre}"></c:out></td>
                                    <td class="actions">
                                        <a href="AdminEditarCategoria?id=<c:out value="${categoria.id}"></c:out>"><i class="fa fa-edit" title="Editar"></i></a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </body>
</html>