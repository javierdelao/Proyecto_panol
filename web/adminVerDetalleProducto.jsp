<%-- 
    Document   : adminVerDetalleProducto
    Created on : 22-06-2017, 2:24:51
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="text-left logotipo">
                <img src="./img/logo_duoc.png">
            </div>
            <div class="text-right nombre-sistema">
                <span>SISPA</span>
            </div>
        </header>
        <div class="main-container">
            <aside class="aside">
                <ul class="navigation-menu">
                    <li>
                        <a href="AdminListadoProductos" class="active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Productos
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoCategorias">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Categorías
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoMarcaProveedor">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Marca / Proveedor
                        </a>
                    </li>
                    <li>
                        <a href="Logout">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Salir
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="main-content">
                <div class="mensaje-bienvenida">
                    <c:set var="nombreUserLog" scope="session" value="${sessionScope.nombreUserLog}"></c:set>
                    Hola, <c:out value="${nombreUserLog}"></c:out>
                </div>
                <div class="main-heading">
                    <h1>Ver Detalle / Stock de "<c:out value="${subcategoria.nombre}"></c:out>"</h1>
                </div>
                <div class="content">
                    <a href="AdminListadoProductos" class="btn-gray" style="margin-bottom: 20px; display: inline-block;"> < Volver</a>
                    <a href="AdminAgregarUnidadProducto?subcategoria=<c:out value="${subcategoria.id}"></c:out>" class="btn-yellow" style="margin-bottom: 20px; display: inline-block;">Agregar unidad</a>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Imagen</th>
                                <th>Marca / Proveedor</th>
                                <th>Estado</th>
                                <th>Observación</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="producto" items="${listadoProductos}" >
                                <tr>
                                    <td><c:out value="${producto.id}"></c:out></td>
                                    <td></td>
                                    <td><c:out value="${listadoMarcaProveedor.get( (producto.marca_proveedor_id - 1 )).nombre}"></c:out></td>
                                    <td><c:out value="${listadoEstados.get( (producto.estado_producto_id - 1 )).descripcion}"></c:out></td>
                                    <td><c:out value="${producto.observacion}"></c:out></td>
                                    <td class="text-center">
                                        <a href="AdminEditarUnidadProducto?subcategoria=<c:out value="${subcategoria.id}"></c:out>&producto=<c:out value="${producto.id}"></c:out>"><i class="fa fa-edit"></i></a>&nbsp;
                                        <a href="AdminDarBajaUnidad?subcategoria=<c:out value="${subcategoria.id}"></c:out>&producto=<c:out value="${producto.id}"></c:out>"><i class="fa fa-ban"></i></a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </body>
</html>