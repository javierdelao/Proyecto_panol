<%-- 
    Document   : adminEditarProducto
    Created on : 22-06-2017, 2:08:54
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="text-left logotipo">
                <img src="./img/logo_duoc.png">
            </div>
            <div class="text-right nombre-sistema">
                <span>SISPA</span>
            </div>
        </header>
        <div class="main-container">
            <aside class="aside">
                <ul class="navigation-menu">
                    <li>
                        <a href="AdminListadoProductos" class="active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Productos
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoCategorias">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Categorías
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoMarcaProveedor">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Marca / Proveedor
                        </a>
                    </li>
                    <li>
                        <a href="Logout">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Salir
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="main-content">
                <div class="mensaje-bienvenida">
                    <c:set var="nombreUserLog" scope="session" value="${sessionScope.nombreUserLog}"></c:set>
                    Hola, <c:out value="${nombreUserLog}"></c:out>
                </div>
                <div class="main-heading">
                    <h1>Editar Producto</h1>
                </div>
                <div class="content">
                    <div class="login-box">
                        <div class="msg-response"></div>
                        <form action="AdminEditarProducto" method="POST" id="EditarProductoForm">
                            <div class="inputs">
                                <fieldset>
                                    <label>Nombre</label>
                                    <input type="text" name="nombre" value="<c:out value="${subcategoria.nombre}"></c:out>">
                                    <input type="hidden" name="id" value="<c:out value="${subcategoria.id}"></c:out>">
                                </fieldset>
                                <fieldset>
                                    <label>Categoría</label>
                                    <select name="categoria_id">
                                        <option value="">Seleccione</option>
                                        <c:forEach var="categoria" items="${listadoCategorias}" >
                                            <c:choose>
                                                <c:when test="${categoria.id == subcategoria.categoria_id}">
                                                    <option value="<c:out value="${categoria.id}"></c:out>" selected="selected"><c:out value="${categoria.nombre}"></c:out></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="<c:out value="${categoria.id}"></c:out>"><c:out value="${categoria.nombre}"></c:out></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                </fieldset>
                                <fieldset>
                                    <label>Tipo Producto</label>
                                    <select name="tipo_producto_id" id="TipoProducto">
                                        <option value="">Seleccione</option>
                                        <c:forEach var="tipo_producto" items="${listadoTipoProducto}" >
                                            <c:choose>
                                                <c:when test="${tipo_producto.id == subcategoria.tipo_producto_id}">
                                                    <option value="<c:out value="${tipo_producto.id}"></c:out>" selected="selected"><c:out value="${tipo_producto.descripcion}"></c:out></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="<c:out value="${tipo_producto.id}"></c:out>"><c:out value="${tipo_producto.descripcion}"></c:out></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                </fieldset>
                                <fieldset class="hide" id="StockContainer">
                                    <label>Stock</label>
                                    <input type="number" name="stock" value="<c:out value="${subcategoria.stock}"></c:out>">
                                </fieldset>
                            </div>
                            <div class="submit-button">
                                <a href="AdminListadoProductos" class="btn-gray">Cancelar</a>
                                <a href="javascript:;" class="btn-yellow editarProducto">Guardar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".editarProducto").on("click",function(e){
                    e.preventDefault();
                    $("#EditarProductoForm").submit();
                });
                
                function cargaStock(){
                    if( $("#TipoProducto").val() == 2){
                        $("#StockContainer").removeClass("hide");
                    } else {
                        $("#StockContainer").addClass("hide");
                        $("#StockContainer").find("input").val("0");
                    }
                }
                
                $("#TipoProducto").on("change",function(){
                    cargaStock();
                });
                cargaStock();
            });
        </script>
    </body>
</html>