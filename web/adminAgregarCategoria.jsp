<%-- 
    Document   : adminAgregarCategoria
    Created on : 21-06-2017, 23:24:34
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="text-left logotipo">
                <img src="./img/logo_duoc.png">
            </div>
            <div class="text-right nombre-sistema">
                <span>SISPA</span>
            </div>
        </header>
        <div class="main-container">
            <aside class="aside">
                <ul class="navigation-menu">
                    <li>
                        <a href="AdminListadoProductos">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Productos
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoCategorias" class="active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Categorías
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoMarcaProveedor">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Marca / Proveedor
                        </a>
                    </li>
                    <li>
                        <a href="Logout">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Salir
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="main-content">
                <div class="mensaje-bienvenida">
                    <c:set var="nombreUserLog" scope="session" value="${sessionScope.nombreUserLog}"></c:set>
                    Hola, <c:out value="${nombreUserLog}"></c:out>
                </div>
                <div class="main-heading">
                    <h1>Agregar Nueva Categoría</h1>
                </div>
                <div class="content">
                    <div class="login-box">
                        <div class="msg-response"></div>
                        <form action="AdminAgregarCategoria" method="POST" id="AgregarCategoriaForm">
                            <div class="inputs">
                                <fieldset>
                                    <label>Nombre</label>
                                    <input type="text" name="nombre">
                                </fieldset>
                            </div>
                            <div class="submit-button">
                                <a href="AdminListadoCategorias" class="btn-gray">Cancelar</a>
                                <a href="javascript:;" class="btn-yellow agregarCategoria">Crear</a>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".agregarCategoria").on("click",function(e){
                    e.preventDefault();
                    $("#AgregarCategoriaForm").submit();
                });
            });
        </script>
    </body>
</html>