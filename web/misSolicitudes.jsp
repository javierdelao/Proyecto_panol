<%-- 
    Document   : misSolicitudes
    Created on : 22-06-2017, 23:10:21
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="text-left logotipo">
                <img src="./img/logo_duoc.png">
            </div>
            <div class="text-right nombre-sistema">
                <span>SISPA</span>
            </div>
        </header>
        <div class="main-container">
            <aside class="aside">
                <ul class="navigation-menu">
                    <li>
                        <a href="CatalogoProductos">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Catálogo
                        </a>
                    </li>
                    <li>
                        <a href="MisSolicitudes" class="active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Mis Solicitudes
                        </a>
                    </li>
                    <li>
                        <a href="MisPrestamos">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Mis Préstamos
                        </a>
                    </li>
                    <li>
                        <a href="MisDatos">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Mis Datos
                        </a>
                    </li>
                    <li>
                        <a href="Logout">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Salir
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="main-content">
                <div class="mensaje-bienvenida">
                    <c:set var="nombreUserLog" scope="session" value="${sessionScope.nombreUserLog}"></c:set>
                    Hola, <c:out value="${nombreUserLog}"></c:out>
                </div>
                <div class="main-heading">
                    <h1>Mis Solicitudes</h1>
                </div>
                <div class="content">
                    <c:if test="${ solicitudActiva != null }">
                        <style type="text/css">
                            #EnviarSolicitudActiva{
                                margin-bottom: 15px; margin-left: 16px;display: inline-block;
                            }
                        </style>
                        <a href="javascript:;" id="EnviarSolicitudActiva" class="btn-yellow" rel-id="<c:out value="${solicitudActiva.id}"></c:out>">
                            Enviar Solicitud
                        </a>
                    </c:if>
                    
                    <% int count = 1; %>
                    <c:forEach var="solicitud" items="${listadoSolicitudes}">
                        <div class="panel">
                            <div class="panel-heading" role="tab" id="heading<%=count%>">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<%=count%>" aria-expanded="true" aria-controls="collapse<%=count%>">
                                    <div>Código Solicitud: <c:out value="${solicitud.id}"></c:out></div>
                                    <div>Fecha Solicitud: <fmt:formatDate type="date" value="${solicitud.fecha_creacion}"></fmt:formatDate></div>
                                    <div>Estado: <b><c:out value="${ listadoEstadoSolicitud.get( solicitud.estado_solicitud_id-1 ).descripcion }"></c:out></b></div>
                                </a>
                            </div>
                            <div id="collapse<%=count%>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<%=count%>">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Código Producto</th>
                                            <th>Nombre Producto</th>
                                            <th>Cantidad Solicitada</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="relacionSubcategoria" items="${solicitud.listadoProductos}">
                                        <tr>
                                            <td><c:out value="${relacionSubcategoria.subcategoria.id}"></c:out></td>
                                            <td><c:out value="${relacionSubcategoria.subcategoria.nombre}"></c:out></td>
                                            <td><c:out value="${relacionSubcategoria.cantidad_solicitud}"></c:out></td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <% count++; %>
                    </c:forEach>
                </div>
            </section>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $("body").on("click","#EnviarSolicitudActiva",function(e){
                    e.preventDefault();
                    var solicitud = $(this).attr("rel-id");
                    window.location.href = "EnviarSolicitud?solicitud="+solicitud;
                });
            });
        </script>
    </body>
</html>