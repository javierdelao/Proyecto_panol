<%-- 
    Document   : panoleroListadoSolicitudes
    Created on : 23-06-2017, 14:50:16
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="text-left logotipo">
                <img src="./img/logo_duoc.png">
            </div>
            <div class="text-right nombre-sistema">
                <span>SISPA</span>
            </div>
        </header>
        <div class="main-container">
            <aside class="aside">
                <ul class="navigation-menu">
                    <li>
                        <a href="PanoleroListadoSolicitudes" class="active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Solicitudes
                        </a>
                    </li>
                    <li>
                        <a href="PanoleroListadoPrestamos">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Préstamos
                        </a>
                    </li>
                    <li>
                        <a href="PanoleroListadoProductos">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Productos
                        </a>
                    </li>
                    <li>
                        <a href="reportes.html">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Reportes
                        </a>
                    </li>
                    <li>
                        <a href="Logout">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Salir
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="main-content">
                <div class="mensaje-bienvenida">
                    <c:set var="nombreUserLog" scope="session" value="${sessionScope.nombreUserLog}"></c:set>
                    Hola, <c:out value="${nombreUserLog}"></c:out>
                </div>
                <div class="main-heading">
                    <h1>Listado Solicitudes</h1>
                </div>
                <div class="content">
                    <div class="filter-horizontal-box">
                        <form action="" method="POST">
                            <div class="inputs">
                                <fieldset>
                                    <input type="text" name="buscar_codigo" placeholder="Buscar por rut solicitante">
                                </fieldset>
                                <fieldset>
                                    <input type="text" name="buscar_rut" placeholder="Buscar por código">
                                </fieldset>
                            </div>
                            <div class="submit-button">
                                <a href="javascript:;" class="btn-yellow">Filtrar</a>
                            </div>
                        </form>
                    </div>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Solicitante</th>
                                <th>Fecha</th>
                                <th>Estado</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="solicitud" items="${listadoSolicitudes}">
                                <tr>
                                    <td><c:out value="${solicitud.id}"></c:out></td>
                                    <td><c:out value="${solicitud.usuario.nombre}"></c:out>&nbsp;<c:out value="${solicitud.usuario.apellido}"></c:out></td>
                                    <td><fmt:formatDate type="date" value="${solicitud.fecha_creacion}"></fmt:formatDate></td>
                                    <td><c:out value="${ listadoEstadoSolicitud.get( solicitud.estado_solicitud_id -1 ).descripcion }"></c:out></td>
                                    <td class="text-center">
                                        <c:if test="${ solicitud.estado_solicitud_id == 2 }">
                                            <a href="javascript:;" class="btn-yellow btn-xs registrarPrestamo" rel-id="<c:out value="${solicitud.id}"></c:out>">
                                                Generar Préstamo
                                            </a>&nbsp;&nbsp;
                                            <a href="javascript:;" class="btn-gray btn-xs rechazarSolicitud" rel-id="<c:out value="${solicitud.id}"></c:out>">
                                                Rechazar Solicitud
                                            </a>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </section>
            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="modalInfoTitle1" id="generarPrestamoModal">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="modalInfoTitle1">Generar Préstamo</h4>
                        </div> 
                        <div class="modal-body">
                            
                        </div>
                        <div class="modal-footer">
                            <div class="text-left">
                                <a class="btn-gray" href="javascript:;" data-dismiss="modal">Cancelar</a>
                            </div>
                            <div class="text-right">
                                <a class="btn-yellow generarPrestamo" href="javascript:;">Generar Préstamo</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="modalInfoTitle1" id="rechazarSolicitudModal">
                <div class="modal-dialog modal-sm" role="document" style="width: 400px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="modalInfoTitle1">Rechazar Solicitud Cod: <span id="solicitudCod"></span></h4>
                        </div> 
                        <div class="modal-body">
                            <form action="RechazarSolicitud" type="POST" id="RechazarSolicitud">
                                <fieldset>
                                    <label style="display: block;">Observación:</label>
                                    <textarea name="observacion" style="width: 100%;"></textarea>
                                </fieldset>
                                <input type="hidden" name="solicitudId">
                            </form>
                        </div>
                        <div class="modal-footer">
                            <div class="text-left">
                                <a class="btn-gray" href="javascript:;" data-dismiss="modal">Cancelar</a>
                            </div>
                            <div class="text-right">
                                <a class="btn-yellow" id="btnRechazarSolicitud" href="javascript:;">Rechazar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $("body").on("click",".registrarPrestamo",function(e){
                    e.preventDefault();
                    var solicitudId = $(this).attr("rel-id");
                    $.ajax({
                        url: "CargaFormularioGenerarPrestamo",
                        type: "POST",
                        data: { solicitud: solicitudId },
                        success: function(response){
                            $("#generarPrestamoModal .modal-body").html(response);
                            $("#generarPrestamoModal").modal();
                        }
                    });
                });
                
                $("body").on("click",".generarPrestamo",function(e){
                    e.preventDefault();
                    $("body").find("#GenerarPrestamo").submit();
                });
                
                $("body").on("click",".rechazarSolicitud",function(e){
                    e.preventDefault();
                    var solicitudId = $(this).attr("rel-id");
                    $("#solicitudCod").text(solicitudId);
                    $("#RechazarSolicitud").find("input[name='solicitudId']").val(solicitudId);
                    $("#rechazarSolicitudModal").modal();
                });
                
                $("body").on("click","#btnRechazarSolicitud",function(e){
                    e.preventDefault();
                    $("#RechazarSolicitud").submit();
                });
            });
        </script>
    </body>
</html>