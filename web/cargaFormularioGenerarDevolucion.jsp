<%-- 
    Document   : cargaFormularioGenerarDevolucion
    Created on : 30-06-2017, 17:15:46
    Author     : Nicolas Recabarren
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<form action="GenerarDevolucion" type="POST" id="GenerarPrestamo">
    <input type="hidden" value="<c:out value="${solicitud.id}"></c:out>" name="solicitudId">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Código Producto</th>
                <th>Nombre Producto</th>
                <th>Estado</th>
                <th>Observación</th>
            </tr>
        </thead>
        <tbody>
        <c:forEach var="solProds" items="${prestamo.listadoProductos}">
            <c:if test="${solProds.subcategoria.tipo_producto_id == 1}">
                <c:forEach var="detalle" items="${solProds.detalles}">
                    <tr>
                        <td><c:out value="${detalle.producto.id}"></c:out></td>
                        <td><c:out value="${detalle.producto.nombre}"></c:out></td>
                        <td>
                            <select name="estado_producto_id">
                                <option value="1">Bueno</option>
                                <option value="2">Necesita Mantención</option>
                                <option value="3">Dar de baja</option>
                            </select>
                        </td>
                        <td>
                            <textarea name="observacion"></textarea>
                        </td>
                    </tr>
                </c:forEach>
            </c:if>
        </c:forEach>
        </tbody>
    </table>
</form>
