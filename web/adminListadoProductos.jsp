<%-- 
    Document   : adminListadoProductos
    Created on : 22-06-2017, 0:48:32
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="text-left logotipo">
                <img src="./img/logo_duoc.png">
            </div>
            <div class="text-right nombre-sistema">
                <span>SISPA</span>
            </div>
        </header>
        <div class="main-container">
            <aside class="aside">
                <ul class="navigation-menu">
                    <li>
                        <a href="AdminListadoProductos" class="active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Productos
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoCategorias">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Categorías
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoMarcaProveedor">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Marca / Proveedor
                        </a>
                    </li>
                    <li>
                        <a href="Logout">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Salir
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="main-content">
                <div class="mensaje-bienvenida">
                    <c:set var="nombreUserLog" scope="session" value="${sessionScope.nombreUserLog}"></c:set>
                    Hola, <c:out value="${nombreUserLog}"></c:out>
                </div>
                <div class="main-heading">
                    <h1>Listado Productos</h1>
                </div>
                <div class="content">
                    <div class="filter-horizontal-box">
                        <form action="" method="POST">
                            <div class="inputs">
                                <fieldset>
                                    <input type="text" name="buscar_codigo" placeholder="Buscar por código">
                                </fieldset>
                                <fieldset>
                                    <input type="text" name="buscar_nombre" placeholder="Buscar por nombre">
                                </fieldset>
                            </div>
                            <div class="submit-button">
                                <a href="javascript:;" class="btn-yellow">Filtrar</a>
                            </div>
                        </form>
                    </div>
                    <a href="AdminAgregarProducto" class="btn-yellow" style="margin-bottom: 20px; display: inline-block;">Agregar nuevo producto</a>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Categoría</th>
                                <th>Tipo Producto</th>
                                <th>Stock</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="subcategoria" items="${listadoSubcategorias}" >
                                <tr>
                                    <td><c:out value="${subcategoria.id}"></c:out></td>
                                    <td><c:out value="${subcategoria.nombre}"></c:out></td>
                                    <td><c:out value="${listadoCategorias.get( (subcategoria.categoria_id - 1 )).nombre}"></c:out></td>
                                    <td><c:out value="${listadoTipoProducto.get( (subcategoria.tipo_producto_id - 1 )).descripcion}"></c:out></td>
                                    <td><c:out value="${subcategoria.stock}"></c:out> unidades</td>
                                    <td class="text-center">
                                        <a href="AdminEditarProducto?id=<c:out value="${subcategoria.id}"></c:out>"><i class="fa fa-edit"></i></a>&nbsp;
                                        <c:if test="${ subcategoria.tipo_producto_id == 1}">
                                            <a href="AdminVerDetalleProducto?subcategoria=<c:out value="${subcategoria.id}"></c:out>"><i class="fa fa-search"></i></a>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </body>
</html>