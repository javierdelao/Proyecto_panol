<%-- 
    Document   : adminAgregarUnidadProducto
    Created on : 22-06-2017, 9:47:49
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="text-left logotipo">
                <img src="./img/logo_duoc.png">
            </div>
            <div class="text-right nombre-sistema">
                <span>SISPA</span>
            </div>
        </header>
        <div class="main-container">
            <aside class="aside">
                <ul class="navigation-menu">
                    <li>
                        <a href="AdminListadoProductos" class="active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Productos
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoCategorias">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Categorías
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoMarcaProveedor">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Marca / Proveedor
                        </a>
                    </li>
                    <li>
                        <a href="Logout">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Salir
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="main-content">
                <div class="mensaje-bienvenida">
                    <c:set var="nombreUserLog" scope="session" value="${sessionScope.nombreUserLog}"></c:set>
                    Hola, <c:out value="${nombreUserLog}"></c:out>
                </div>
                <div class="main-heading">
                    <h1>Agregar Stock a "<c:out value="${subcategoria.nombre}"></c:out>"</h1>
                </div>
                <div class="content">
                    <div class="login-box">
                        <div class="msg-response"></div>
                        <form action="AdminAgregarUnidadProducto?subcategoria=<c:out value="${subcategoria.id}"></c:out>" method="POST" id="AgregarProductoForm">
                            <div class="inputs">
                                <fieldset>
                                    <label>Nombre</label>
                                    <input type="text" name="nombreDisabled" disabled="disabled" value="<c:out value="${subcategoria.nombre}"></c:out>">
                                    <input type="hidden" name="nombre" value="<c:out value="${subcategoria.nombre}"></c:out>">
                                </fieldset>
                                <fieldset>
                                    <label>Marca / Proveedor</label>
                                    <select name="marca_proveedor_id">
                                        <option value="">Seleccione</option>
                                        <c:forEach var="marca" items="${listadoMarcaProveedor}">
                                            <option value="<c:out value="${marca.id}"></c:out>"><c:out value="${marca.nombre}"></c:out></option>
                                        </c:forEach>
                                    </select>
                                </fieldset>
                                <fieldset>
                                    <label>Estado</label>
                                    <select name="estado_producto_id">
                                        <option value="">Seleccione</option>
                                        <c:forEach var="estado" items="${listadoEstados}">
                                            <option value="<c:out value="${estado.id}"></c:out>"><c:out value="${estado.descripcion}"></c:out></option>
                                        </c:forEach>
                                    </select>
                                </fieldset>
                                <fieldset>
                                    <label>Imagen</label>
                                    <input type="file" name="imagen">
                                </fieldset>
                                <fieldset>
                                    <label>Observación</label>
                                    <textarea name="observacion"></textarea>
                                </fieldset>
                            </div>
                            <div class="submit-button">
                                <a href="AdminVerDetalleProducto?subcategoria=<c:out value="${subcategoria.id}"></c:out>" class="btn-gray">Cancelar</a>
                                <a href="javascript:;" class="btn-yellow agregarProducto">Crear</a>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".agregarProducto").on("click",function(e){
                    e.preventDefault();
                    $("#AgregarProductoForm").submit();
                });
            });
        </script>
    </body>
</html>