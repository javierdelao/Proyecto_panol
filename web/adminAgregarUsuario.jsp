<%-- 
    Document   : adminAgregarUsuario
    Created on : 29-06-2017, 16:41:17
    Author     : DuduRengo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="text-left logotipo">
                <img src="./img/logo_duoc.png">
            </div>
            <div class="text-right nombre-sistema">
                <span>SISPA</span>
            </div>
        </header>
        <div class="main-container">
            <aside class="aside">
                <ul class="navigation-menu">
                    <li>
                        <a href="AdminListadoUsuarios" class="active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Usuarios
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoProductos">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Productos
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoCategorias">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Categorías
                        </a>
                    </li>
                    <li>
                        <a href="AdminListadoMarcaProveedor">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Marca / Proveedor
                        </a>
                    </li>
                    <li>
                        <a href="Logout">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Salir
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="main-content">
                <div class="mensaje-bienvenida">
                    <c:set var="nombreUserLog" scope="session" value="${sessionScope.nombreUserLog}"></c:set>
                    Hola, <c:out value="${nombreUserLog}"></c:out>
                </div>
                <div class="main-heading">
                    <h1>Agregar Nuevo Usuario</h1>
                </div>
                <div class="content">
                    <div class="login-box">
                        <div class="msg-response"></div>
                        <form id="AgregarUsuarioForm" action="Login" method="POST">
                            <input type="hidden" value="register" name="action">
                            <div class="inputs">
                                <fieldset>
                                    <label>Rut</label>
                                    <input type="text" name="rut">
                                </fieldset>
                                <fieldset>
                                    <label>Nombre</label>
                                    <input type="text" name="nombre">
                                </fieldset>
                                <fieldset>
                                    <label>Apellido</label>
                                    <input type="text" name="apellido">
                                </fieldset>
                                <fieldset>
                                    <label>Domicilio/Dirección</label>
                                    <input type="text" name="domicilio">
                                </fieldset>
                                <fieldset>
                                    <label>Teléfono</label>
                                    <input type="text" name="telefono">
                                </fieldset>
                                <fieldset>
                                    <label>Correo</label>
                                    <input type="email" name="correo">
                                </fieldset>
                                <fieldset>
                                    <label>Sede</label>
                                    <select name="sede" id="sede" onchange="cargaEscuelas()">
                                        <c:forEach var="sede" items="${listadoSedes}" >
                                            <option value="<c:out value="${sede.id}"></c:out>"><c:out value="${sede.nombre}"></c:out></option>
                                        </c:forEach>
                                        
                                        
                                    </select>
                                </fieldset>
                                <fieldset>
                                    <label>Escuela</label>
                                    <select name="escuela" id="escuela">
                                        <option value="">seleccione</option>
                                    </select>
                                </fieldset>
                                <fieldset>
                                    <label>Tipo Usuario</label>
                                    <select name="tipo_usuario">
                                        <option value="2">Alumno</option>
                                        <option value="3">Profesor</option>
                                    </select>
                                </fieldset>
                            </div>
                            <div class="submit-button">
                                <a href="Login" class="btn-gray">Cancelar</a>
                                <a class="btn-yellow agregarUsuario" href="javascript:;">Registrar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <script type="text/javascript">
            function cargaEscuelas(){
                var sedeId=$('#sede').val();
                console.log(sedeId);
                 $.ajax({
            url : 'Login',
            type : 'post',
            data : {action : 'selectSede',idSede:sedeId},
            success : function(r){
            console.log(r);
            var options="";
            for(var i=0;i<r.length;i++){
                options+='<option value="'+r[i].id+'">'+r[i].nombre+'</option>';
            }
            $('#escuela').html(options);
                
            },
            error : function(error){
                console.log('error');
                console.log(error);
            }
        });    
            }
                $(document).ready(function(){
                $(".agregarUsuario").on("click",function(e){
                    e.preventDefault();
                    $("#AgregarUsuarioForm").submit();
                });
            });
            
        </script>
    </body>
</html>