<%-- 
    Document   : login
    Created on : 21/06/2017, 04:18:55 PM
    Author     : javier
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="text-left logotipo">
                <img src="./img/logo_duoc.png">
            </div>
            <div class="text-right nombre-sistema">
                <span>SISPA</span>
            </div>
        </header>
        <div class="main-container">
            <aside class="aside">
                <ul class="navigation-menu">
                    <li>
                        <a href="Login" class="active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Ingresar
                        </a>
                    </li>
                    <li>
                        <a href="Login?action=registro">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Registro
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="main-content">
                <div class="main-heading">
                    <h1>INGRESAR</h1>
                </div>
                <div class="content">
                    <div class="login-box">
                        <c:set var="msg_response" scope="session" value="${sessionScope.msg_response}"></c:set>
                        <c:if test="${ msg_response != null && !msg_response.isEmpty() }">
                            
                            <c:set var="class_msg_response" scope="session" value="${sessionScope.class_msg_response}"></c:set>
                            <div class="msg-response <c:out value="${class_msg_response}"></c:out>">
                                <c:out value="${msg_response}"></c:out>
                            </div>
                            <c:set var="msg_response" value="${null}" scope="session"></c:set>
                        </c:if>
                        
                        
                        
                        <form action="Login" method="POST" id="LoginForm">
                            <input type="hidden" value="login" name="action"> 
                            <div class="inputs">
                                <fieldset>
                                    <label>Rut</label>
                                    <input type="text" name="rut">
                                </fieldset>
                                <fieldset>
                                    <label>Contraseña</label>
                                    <input type="password" name="password">
                                </fieldset>
                            </div>
                            <div class="submit-button">
                                <a class="btn-yellow" id="btnIngresar" href="javascript:;">Ingresar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#btnIngresar").on("click",function(e){
                    e.preventDefault();
                    var error = 0;
                    if($("input[name='rut']").val() == ""){
                        $("input[name='rut']").css("border-color","red");
                        error++;
                    }
                    if($("input[name='password']").val() == ""){
                        $("input[name='password']").css("border-color","red");
                        error++;
                    }
                    if(error == 0){
                        $("#LoginForm").submit();
                    } else {
                        alert("Debe completar todos los campos.");
                        return false;
                    }
                });
            });
        </script>
    </body>
</html>