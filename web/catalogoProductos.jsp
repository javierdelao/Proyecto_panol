<%-- 
    Document   : catalogoProductos
    Created on : 22-06-2017, 13:14:53
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
        <script src="./js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="./js/funciones.js" type="text/javascript"></script>
    </head>
    <body>
        <header>
            <div class="text-left logotipo">
                <img src="./img/logo_duoc.png">
            </div>
            <div class="text-right nombre-sistema">
                <span>SISPA</span>
            </div>
        </header>
        <div class="main-container">
            <aside class="aside">
                <ul class="navigation-menu">
                    <li>
                        <a href="CatalogoProductos" class="active">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Catálogo
                        </a>
                    </li>
                    <li>
                        <a href="MisSolicitudes">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Mis Solicitudes
                        </a>
                    </li>
                    <li>
                        <a href="MisPrestamos">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Mis Préstamos
                        </a>
                    </li>
                    <li>
                        <a href="MisDatos">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Mis Datos
                        </a>
                    </li>
                    <li>
                        <a href="Logout">
                            <i class="fa fa-angle-right" aria-hidden="true"></i> Salir
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="main-content">
                <div class="mensaje-bienvenida">
                    <c:set var="nombreUserLog" scope="session" value="${sessionScope.nombreUserLog}"></c:set>
                    Hola, <c:out value="${nombreUserLog}"></c:out>
                </div>
                <div class="main-heading">
                    <h1>Catálogo de Productos</h1>
                </div>
                <div class="content">
                    <div class="filter-horizontal-box">
                        <form action="CatalogoProductos" method="POST" id="FiltrarProductos">
                            <div class="inputs">
                                <fieldset>
                                    <c:if test="${ nombreFiltro.isEmpty() }">
                                        <input type="text" name="nombre" placeholder="Buscar...">
                                    </c:if>
                                    <c:if test="${ !nombreFiltro.isEmpty() }">
                                        <input type="text" name="nombre" placeholder="Buscar..." value="<c:out value="${nombreFiltro}"></c:out>">
                                    </c:if>
                                    
                                </fieldset>
                                <fieldset>
                                    <select name="categoria_id">
                                        <option value="">Seleccione</option>
                                        <c:forEach var="categoria" items="${listadoCategorias}" >
                                            <c:choose>
                                                <c:when test="${!categoriaFiltro.isEmpty() && categoria.id == Integer.parseInt(categoriaFiltro)}">
                                                    <option value="<c:out value="${categoria.id}"></c:out>" selected="selected"><c:out value="${categoria.nombre}"></c:out></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="<c:out value="${categoria.id}"></c:out>"><c:out value="${categoria.nombre}"></c:out></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                </fieldset>
                            </div>
                            <div class="submit-button">
                                <a href="javascript:;" class="btn-yellow" id="btnFiltrar">Filtrar</a>
                            </div>
                        </form>
                    </div>
                    <ul class="products-list">
                        <c:forEach var="producto" items="${listadoSubcategorias}">
                            <li>
                                <div class="product-img text-center">
                                    <img src="./img/notebook_pañol.jpg">
                                </div>
                                <div class="product-details">
                                    <p class="title"><c:out value="${producto.nombre}"></c:out></p>
                                    <p>
                                        Código: <c:out value="${producto.id}"></c:out> <br />
                                        Stock: <c:out value="${producto.stock}"></c:out> unidades.
                                    </p>
                                    <c:choose>
                                        <c:when test="${producto.stock == 0}">
                                            <a href="javascript:;" class="btn-yellow disabled" rel-id="<c:out value="${producto.id}"></c:out>" rel-stock="<c:out value="${producto.stock}"></c:out>">No Disponible</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="javascript:;" class="btn-yellow solicitarProducto" rel-id="<c:out value="${producto.id}"></c:out>" rel-stock="<c:out value="${producto.stock}"></c:out>">Solicitar</a>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </section>
        </div>
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="modalInfoTitle1" id="solicitarProductoModal">
            <div class="modal-dialog modal-sm" role="document" style="width: 370px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="modalInfoTitle1">Stock</h4>
                    </div> 
                    <div class="modal-body">
                        Indique la cantidad de productos que desea solicitar: <input type="number" name="cantidadSolicitada" id="CantidadSolicitada" value="0">
                        <i class="fa fa-spin fa-spinner fa-fx hide"></i>
                        <input type="hidden" name="productoSolicitado" id="ProductoSolicitado">
                        <c:set var="perfil" scope="session" value="${sessionScope.usuario_perfil}"></c:set>
                        <div class="<c:out value="${perfil != 3 ? 'hide' : ''}"></c:out>">
                            <br />
                            <label>Fecha de Préstamo:</label><br />
                            <input type="text" name="fecha_prestamo" class="datePicker" id="FechaPrestamo">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="text-left">
                            <a class="btn-gray" href="javascript:;" data-dismiss="modal">Cancelar</a>
                        </div>
                        <div class="text-right">
                            <a class="btn-yellow solicitarProductoStock" href="javascript:;">Solicitar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $("body").on("click","#btnFiltrar",function(e){
                    e.preventDefault();
                    $("#FiltrarProductos").submit();
                });
                
                $("body").on("click",".solicitarProducto",function(){
                    var productoId = $(this).attr("rel-id");
                    
                    $("#solicitarProductoModal").find("#ProductoSolicitado").val(productoId);
                    $("#solicitarProductoModal").find("#CantidadSolicitada").val(0);
                    $("#solicitarProductoModal").modal();
                });
                
                $("body").on("click",".solicitarProductoStock",function(e){
                    e.preventDefault();
                    var error = 0;
                    var inputCantidad = $("#solicitarProductoModal").find("#CantidadSolicitada");
                    if( inputCantidad.val() == "" || inputCantidad.val() == 0){
                        if( !inputCantidad.hasClass('has-error') ){
                            inputCantidad.addClass('has-error');
                        }
                        error++;
                    }else{
                        inputCantidad.removeClass('has-error');
                    }
                    
                    var inputFechaPrestamo = $('#solicitarProductoModal').find('#FechaPrestamo');
                    <c:if test="${ perfil == 3 }">
                        if( inputFechaPrestamo.val() == ""){
                            if( !inputFechaPrestamo.hasClass('has-error')){
                                inputFechaPrestamo.addClass('has-error');
                            }
                            error++;
                        } else {
                            inputFechaPrestamo.removeClass('has-error');
                        }
                    </c:if>
                    
                    if(error == 0){
                        $.ajax({
                            url: "ValidaCantidadSolicitada",
                            data: {
                                productoSolicitado: $("#solicitarProductoModal").find("#ProductoSolicitado").val(),
                                cantidadSolicitada: inputCantidad.val()
                            },
                            type: "POST",
                            beforeSend: function(){
                                $("#solicitarProductoModal").find('.fa-spin').removeClass('hide');
                            },
                            success: function(response){
                                if( $.trim(response) == "true" ){
                                    inputCantidad.removeClass("has-error");
                                    inputCantidad.addClass("has-success");
                                    $("#solicitarProductoModal").find('.fa-spin').addClass('hide');
                                    agregaProductosASolicitud();
                                } else {
                                    inputCantidad.removeClass("has-success");
                                    inputCantidad.addClass("has-error");
                                    inputCantidad.val("0");
                                    $("#solicitarProductoModal").find('.fa-spin').addClass('hide');
                                    alert("No puede solicitar más productos de los que hay disponibles.");
                                    return false;
                                }
                            }
                        });
                    } else {
                        alert("Antes de continuar debe llenar todos los campos en rojo.");
                    }
                });
                
                function agregaProductosASolicitud(){
                    $.ajax({
                        url: "AgregarProductoASolicitud",
                        data: {
                            producto: $("#solicitarProductoModal").find("#ProductoSolicitado").val(),
                            stock: $("#solicitarProductoModal").find("#CantidadSolicitada").val(),
                            fecha_prestamo: $('#solicitarProductoModal').find('#FechaPrestamo').val()
                        },
                        type: "POST",
                        success: function(response){
                            if( $.trim(response) == "ok" ){
                                alert("Producto agregado a la solicitud activa");
                                $('#solicitarProductoModal').modal('hide');
                                location.reload();
                            } else {
                                alert("No se pudo agregar el producto a la solicitud");
                            }
                        }
                    });
                }
            });
        </script>
    </body>
</html>