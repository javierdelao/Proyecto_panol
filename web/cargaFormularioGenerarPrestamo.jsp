<%-- 
    Document   : cargaFormularioGenerarPrestamo
    Created on : 23-06-2017, 15:50:17
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<div class="panel">
    <div class="panel-heading" role="tab" id="heading1">
        <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
            <div>Código Solicitud: <c:out value="${solicitud.id}"></c:out></div>
            <div>Fecha Solicitud: <fmt:formatDate type="date" value="${solicitud.fecha_creacion}"></fmt:formatDate></div>
            <div style="text-align: right;">Estado: <b><c:out value="${ listadoEstadoSolicitud.get( solicitud.estado_solicitud_id-1 ).descripcion }"></c:out></b></div>
        </a>
    </div>
    <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
        <form action="GenerarPrestamo" type="POST" id="GenerarPrestamo">
            <input type="hidden" value="<c:out value="${solicitud.id}"></c:out>" name="solicitudId">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Código Producto</th>
                        <th>Nombre Producto</th>
                        <th>Cantidad Solicitada</th>
                        <th>Cantidad A Prestar</th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach var="relacionSubcategoria" items="${solicitud.listadoProductos}">
                    <tr>
                        <td><c:out value="${relacionSubcategoria.subcategoria.id}"></c:out></td>
                        <td><c:out value="${relacionSubcategoria.subcategoria.nombre}"></c:out></td>
                        <td><c:out value="${relacionSubcategoria.cantidad_solicitud}"></c:out></td>
                        <td>
                            <input type="hidden" name="subcategoriaIds" value="<c:out value="${relacionSubcategoria.subcategoria.id}"></c:out>">
                            <input type="hidden" name="relaciones" value="<c:out value="${relacionSubcategoria.id}"></c:out>">
                            <input type="number" name="cantidad_prestada" value="0">
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("input[name='cantidadesSolicitadas']").blur(function(){
            var $this = $(this);
            $.ajax({
                url: "ValidaCantidadAPrestar",
                data: { 
                    subcategoriaId: $this.siblings("input[name='subcategoriaIds']").val(),
                    cantidadAPrestar: $this.val()
                },
                type: "POST",
                success: function(response){
                    if( $.trim(response) == "false"){
                        $this.val("0");
                        alert("Error. Está asignando más productos de los que hay disponibles.");
                    }
                }
            });
        });
        
        $('.generarPrestamo').on('click',function(e){
            e.preventDefault();
            $('#GenerarPrestamo').find('input[name="cantidad_prestada"]').each(function(){
                if( $(this).val() == "" ){
                    if( !$(this).hasClass('has-error') ){
                        $(this).addClass('has-error');
                    }
                    error++;
                } else {
                    $(this).removeClass('has-error');
                    $(this).addClass('has-success');
                }
            });
            if(error == 0){
                $('#GenerarPrestamo').submit();
            } else {
                alert("Aún faltan campos por llenar.");
                return false;
            }
        });
    });
</script>