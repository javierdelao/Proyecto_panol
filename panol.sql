/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : panol

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-06-23 00:14:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for categoria
-- ----------------------------
DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of categoria
-- ----------------------------
INSERT INTO `categoria` VALUES ('1', 'Notebooks');
INSERT INTO `categoria` VALUES ('2', 'Cables y Conectores');

-- ----------------------------
-- Table structure for detalle_solicitud
-- ----------------------------
DROP TABLE IF EXISTS `detalle_solicitud`;
CREATE TABLE `detalle_solicitud` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Solicitud_Productos_id` int(11) unsigned NOT NULL,
  `Producto_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`Solicitud_Productos_id`,`Producto_id`),
  KEY `Solicitud_Productos_id` (`Solicitud_Productos_id`),
  KEY `Producto_id` (`Producto_id`),
  CONSTRAINT `detalle_solicitud_ibfk_1` FOREIGN KEY (`Solicitud_Productos_id`) REFERENCES `solicitud_productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detalle_solicitud_ibfk_2` FOREIGN KEY (`Producto_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detalle_solicitud
-- ----------------------------

-- ----------------------------
-- Table structure for escuela
-- ----------------------------
DROP TABLE IF EXISTS `escuela`;
CREATE TABLE `escuela` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `Sede_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`Sede_id`),
  KEY `fk_Escuela_Sede1_idx` (`Sede_id`),
  CONSTRAINT `fk_Escuela_Sede1` FOREIGN KEY (`Sede_id`) REFERENCES `sede` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of escuela
-- ----------------------------
INSERT INTO `escuela` VALUES ('1', 'Construcción', '1');
INSERT INTO `escuela` VALUES ('2', 'Ingeniería', '1');
INSERT INTO `escuela` VALUES ('3', 'Administración y Negocios', '2');
INSERT INTO `escuela` VALUES ('4', 'Diseño', '2');
INSERT INTO `escuela` VALUES ('5', 'Construcción', '2');
INSERT INTO `escuela` VALUES ('6', 'Administración y Negocios', '3');
INSERT INTO `escuela` VALUES ('7', 'Informática y Telecomunicaciones', '3');
INSERT INTO `escuela` VALUES ('8', 'Turismo', '3');
INSERT INTO `escuela` VALUES ('9', 'Administración y Negocios', '4');
INSERT INTO `escuela` VALUES ('10', 'Construcción', '4');
INSERT INTO `escuela` VALUES ('11', 'Informática y Telecomunicaciones', '4');
INSERT INTO `escuela` VALUES ('12', 'Ingeniería', '4');
INSERT INTO `escuela` VALUES ('13', 'Salud', '4');
INSERT INTO `escuela` VALUES ('14', 'Administración y Negocios', '5');
INSERT INTO `escuela` VALUES ('15', 'Informática y Telecomunicaciones', '5');
INSERT INTO `escuela` VALUES ('16', 'Ingeniería', '5');
INSERT INTO `escuela` VALUES ('17', 'Administración y Negocios', '6');
INSERT INTO `escuela` VALUES ('18', 'Informática y Telecomunicaciones', '6');
INSERT INTO `escuela` VALUES ('20', 'Turismo', '6');
INSERT INTO `escuela` VALUES ('21', 'Administración y Negocios', '7');
INSERT INTO `escuela` VALUES ('22', 'Informática y Telecomunicaciones', '7');
INSERT INTO `escuela` VALUES ('23', 'Ingeniería', '7');
INSERT INTO `escuela` VALUES ('24', 'Administración y Negocios', '8');
INSERT INTO `escuela` VALUES ('25', 'Diseño', '8');
INSERT INTO `escuela` VALUES ('26', 'Informática y Telecomunicaciones', '8');
INSERT INTO `escuela` VALUES ('27', 'Administración y Negocios', '9');
INSERT INTO `escuela` VALUES ('28', 'Comunicación', '9');
INSERT INTO `escuela` VALUES ('29', 'Diseño', '9');
INSERT INTO `escuela` VALUES ('30', 'Administración y Negocios', '10');
INSERT INTO `escuela` VALUES ('31', 'Construcción', '10');
INSERT INTO `escuela` VALUES ('32', 'Informática y Telecomunicaciones', '10');
INSERT INTO `escuela` VALUES ('33', 'Ingeniería', '10');
INSERT INTO `escuela` VALUES ('34', 'Recursos Naturales', '10');
INSERT INTO `escuela` VALUES ('35', 'Salud', '10');
INSERT INTO `escuela` VALUES ('36', 'Administración y Negocios', '11');
INSERT INTO `escuela` VALUES ('37', 'Comunicación', '11');
INSERT INTO `escuela` VALUES ('38', 'Construcción', '11');
INSERT INTO `escuela` VALUES ('39', 'Diseño', '11');
INSERT INTO `escuela` VALUES ('40', 'Informática y Telecomunicaciones', '11');
INSERT INTO `escuela` VALUES ('41', 'Ingeniería', '11');
INSERT INTO `escuela` VALUES ('42', 'Salud', '11');
INSERT INTO `escuela` VALUES ('43', 'Turismo', '11');
INSERT INTO `escuela` VALUES ('44', 'Administración y Negocios', '12');
INSERT INTO `escuela` VALUES ('45', 'Construcción', '12');
INSERT INTO `escuela` VALUES ('46', 'Informática y Telecomunicaciones', '12');
INSERT INTO `escuela` VALUES ('47', 'Ingeniería', '12');
INSERT INTO `escuela` VALUES ('48', 'Recursos Naturales', '12');
INSERT INTO `escuela` VALUES ('49', 'Salud', '12');
INSERT INTO `escuela` VALUES ('50', 'Administración y Negocios', '13');
INSERT INTO `escuela` VALUES ('51', 'Comunicación', '13');
INSERT INTO `escuela` VALUES ('52', 'Diseño', '13');
INSERT INTO `escuela` VALUES ('53', 'Salud', '13');
INSERT INTO `escuela` VALUES ('54', 'Informática y Telecomunicaciones', '14');
INSERT INTO `escuela` VALUES ('55', 'Ingeniería', '14');
INSERT INTO `escuela` VALUES ('56', 'Salud', '14');
INSERT INTO `escuela` VALUES ('57', 'Administración y Negocios', '15');
INSERT INTO `escuela` VALUES ('58', 'Construcción', '15');
INSERT INTO `escuela` VALUES ('59', 'Ingeniería', '15');

-- ----------------------------
-- Table structure for estado_producto
-- ----------------------------
DROP TABLE IF EXISTS `estado_producto`;
CREATE TABLE `estado_producto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of estado_producto
-- ----------------------------
INSERT INTO `estado_producto` VALUES ('1', 'Bueno');
INSERT INTO `estado_producto` VALUES ('2', 'Mantención');
INSERT INTO `estado_producto` VALUES ('3', 'Baja');
INSERT INTO `estado_producto` VALUES ('4', 'Prestado');
INSERT INTO `estado_producto` VALUES ('5', 'Solicitado');

-- ----------------------------
-- Table structure for estado_solicitud
-- ----------------------------
DROP TABLE IF EXISTS `estado_solicitud`;
CREATE TABLE `estado_solicitud` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of estado_solicitud
-- ----------------------------
INSERT INTO `estado_solicitud` VALUES ('1', 'Activa');
INSERT INTO `estado_solicitud` VALUES ('2', 'Enviada');
INSERT INTO `estado_solicitud` VALUES ('3', 'Recibida');
INSERT INTO `estado_solicitud` VALUES ('4', 'Aprobada');
INSERT INTO `estado_solicitud` VALUES ('5', 'Rechazada');

-- ----------------------------
-- Table structure for marca_proveedor
-- ----------------------------
DROP TABLE IF EXISTS `marca_proveedor`;
CREATE TABLE `marca_proveedor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of marca_proveedor
-- ----------------------------
INSERT INTO `marca_proveedor` VALUES ('1', 'Dell');
INSERT INTO `marca_proveedor` VALUES ('2', 'HP');

-- ----------------------------
-- Table structure for perfil
-- ----------------------------
DROP TABLE IF EXISTS `perfil`;
CREATE TABLE `perfil` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of perfil
-- ----------------------------
INSERT INTO `perfil` VALUES ('1', 'Pañolero');
INSERT INTO `perfil` VALUES ('2', 'Alumno');
INSERT INTO `perfil` VALUES ('3', 'Profesor');
INSERT INTO `perfil` VALUES ('4', 'Invitado Sede');
INSERT INTO `perfil` VALUES ('5', 'Director');
INSERT INTO `perfil` VALUES ('6', 'Administrador');

-- ----------------------------
-- Table structure for producto
-- ----------------------------
DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `observacion` longtext,
  `imagen` varchar(255) NOT NULL,
  `Marca_Proveedor_id` int(10) unsigned NOT NULL,
  `Subcategoria_id` int(10) unsigned NOT NULL,
  `Estado_Producto_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`Marca_Proveedor_id`,`Subcategoria_id`),
  KEY `fk_Producto_Marca_Proveedor1_idx` (`Marca_Proveedor_id`),
  KEY `fk_Producto_Subcategoria1_idx` (`Subcategoria_id`),
  KEY `fk_Producto_Estado_Producto1_idx` (`Estado_Producto_id`),
  KEY `id` (`id`),
  CONSTRAINT `fk_Producto_Estado_Producto1` FOREIGN KEY (`Estado_Producto_id`) REFERENCES `estado_producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Producto_Marca_Proveedor1` FOREIGN KEY (`Marca_Proveedor_id`) REFERENCES `marca_proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Producto_Subcategoria1` FOREIGN KEY (`Subcategoria_id`) REFERENCES `subcategoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of producto
-- ----------------------------
INSERT INTO `producto` VALUES ('1', 'Notebook i5 8GB RAM 500GB HDD', 'Producto nuevo.', '', '2', '1', '3');
INSERT INTO `producto` VALUES ('2', 'Notebook i5 8GB RAM 500GB HDD', 'Producto nuevo.', '', '2', '1', '1');

-- ----------------------------
-- Table structure for sede
-- ----------------------------
DROP TABLE IF EXISTS `sede`;
CREATE TABLE `sede` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sede
-- ----------------------------
INSERT INTO `sede` VALUES ('1', 'Campus Arauco');
INSERT INTO `sede` VALUES ('2', 'Alameda');
INSERT INTO `sede` VALUES ('3', 'Antonio Varas');
INSERT INTO `sede` VALUES ('4', 'Maipú');
INSERT INTO `sede` VALUES ('5', 'Melipilla');
INSERT INTO `sede` VALUES ('6', 'Padre Alonso de Ovalle');
INSERT INTO `sede` VALUES ('7', 'Plaza Norte');
INSERT INTO `sede` VALUES ('8', 'Plaza Oeste');
INSERT INTO `sede` VALUES ('9', 'Plaza Vespucio');
INSERT INTO `sede` VALUES ('10', 'Puente Alto');
INSERT INTO `sede` VALUES ('11', 'San Andrés de Concepción');
INSERT INTO `sede` VALUES ('12', 'San Bernardo');
INSERT INTO `sede` VALUES ('13', 'San Carlos de Apoquindo');
INSERT INTO `sede` VALUES ('14', 'San Joaquín');
INSERT INTO `sede` VALUES ('15', 'Valparaíso');
INSERT INTO `sede` VALUES ('16', 'Viña del Mar');

-- ----------------------------
-- Table structure for solicitud
-- ----------------------------
DROP TABLE IF EXISTS `solicitud`;
CREATE TABLE `solicitud` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Usuario_id` int(10) unsigned NOT NULL,
  `Tipo_Solicitud_id` int(10) unsigned NOT NULL,
  `Estado_Solicitud_id` int(10) unsigned NOT NULL,
  `observacion` longtext,
  `Receptor_id` int(10) unsigned NOT NULL,
  `fecha_creacion` date NOT NULL,
  PRIMARY KEY (`id`,`Usuario_id`,`Tipo_Solicitud_id`,`Estado_Solicitud_id`,`Receptor_id`),
  KEY `fk_Solicitud_Usuario1_idx` (`Usuario_id`),
  KEY `fk_Solicitud_Tipo_Solicitud1_idx` (`Tipo_Solicitud_id`),
  KEY `fk_Solicitud_Estado_Solicitud1_idx` (`Estado_Solicitud_id`),
  CONSTRAINT `fk_Solicitud_Estado_Solicitud1` FOREIGN KEY (`Estado_Solicitud_id`) REFERENCES `estado_solicitud` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Solicitud_Tipo_Solicitud1` FOREIGN KEY (`Tipo_Solicitud_id`) REFERENCES `tipo_solicitud` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Solicitud_Usuario1` FOREIGN KEY (`Usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of solicitud
-- ----------------------------
INSERT INTO `solicitud` VALUES ('2', '2', '1', '1', '', '0', '0000-00-00');

-- ----------------------------
-- Table structure for solicitud_productos
-- ----------------------------
DROP TABLE IF EXISTS `solicitud_productos`;
CREATE TABLE `solicitud_productos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cantidad_solicitada` int(11) NOT NULL,
  `Solicitud_id` int(10) unsigned NOT NULL,
  `Subcategoria_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`Solicitud_id`,`Subcategoria_id`),
  KEY `fk_Solicitud_Productos_Solicitud1_idx` (`Solicitud_id`),
  KEY `fk_Solicitud_Productos_Producto1_idx` (`Subcategoria_id`),
  KEY `id` (`id`),
  CONSTRAINT `fk_Solicitud_Productos_Solicitud1` FOREIGN KEY (`Solicitud_id`) REFERENCES `solicitud` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Solicitud_Productos_Subcategoria1` FOREIGN KEY (`Subcategoria_id`) REFERENCES `subcategoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of solicitud_productos
-- ----------------------------
INSERT INTO `solicitud_productos` VALUES ('5', '0', '2', '1');

-- ----------------------------
-- Table structure for subcategoria
-- ----------------------------
DROP TABLE IF EXISTS `subcategoria`;
CREATE TABLE `subcategoria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `Categoria_id` int(10) unsigned NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '0',
  `Tipo_Producto_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`Categoria_id`),
  KEY `fk_Subcategoria_Categoria1_idx` (`Categoria_id`),
  KEY `id` (`id`),
  CONSTRAINT `fk_Subcategoria_Categoria1` FOREIGN KEY (`Categoria_id`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of subcategoria
-- ----------------------------
INSERT INTO `subcategoria` VALUES ('1', 'Notebook i5 8GB RAM 500GB HDD', '1', '1', '1');
INSERT INTO `subcategoria` VALUES ('2', 'Conector RJ-45', '2', '10', '2');

-- ----------------------------
-- Table structure for tipo_producto
-- ----------------------------
DROP TABLE IF EXISTS `tipo_producto`;
CREATE TABLE `tipo_producto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_producto
-- ----------------------------
INSERT INTO `tipo_producto` VALUES ('1', 'Retornable');
INSERT INTO `tipo_producto` VALUES ('2', 'Insumo');

-- ----------------------------
-- Table structure for tipo_solicitud
-- ----------------------------
DROP TABLE IF EXISTS `tipo_solicitud`;
CREATE TABLE `tipo_solicitud` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_solicitud
-- ----------------------------
INSERT INTO `tipo_solicitud` VALUES ('1', 'Solicitud');
INSERT INTO `tipo_solicitud` VALUES ('2', 'Préstamo');

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rut` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `domicilio` varchar(255) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `password` varchar(45) NOT NULL,
  `Escuela_id` int(10) unsigned NOT NULL,
  `Perfil_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`Escuela_id`,`Perfil_id`),
  KEY `fk_Usuario_Escuela_idx` (`Escuela_id`),
  KEY `fk_Usuario_Perfil1_idx` (`Perfil_id`),
  CONSTRAINT `fk_Usuario_Escuela` FOREIGN KEY (`Escuela_id`) REFERENCES `escuela` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuario_Perfil1` FOREIGN KEY (`Perfil_id`) REFERENCES `perfil` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('1', '22444663-2', 'Nicolas', 'Recabarren', 'Padre Sergio Correa Gac 7292', '9 4942 1610', 'n.recabarren@alumnos.duoc.cl', 'abc', '7', '6');
INSERT INTO `usuario` VALUES ('2', '1', 'Pepe', 'Perez', 'Casa 123', '123', 'asd@as.d', 'abc', '7', '3');
