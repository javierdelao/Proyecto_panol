/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAO.DAOSubcategoria;
import DAO.DAOSolicitudProductos;
import DAO.DAOSolicitud;
import DAO.DAODetalleSolicitud;
import DAO.DAOProducto;
import Entities.Subcategoria;
import Entities.SolicitudProductos;
import Entities.Solicitud;
import Entities.DetalleSolicitud;
import Entities.Producto;
import com.sun.istack.internal.logging.Logger;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Nicolas Recabarren
 */
@WebServlet(name = "GenerarPrestamo", urlPatterns = {"/GenerarPrestamo"})
public class GenerarPrestamo extends HttpServlet {
    
    Logger logger=Logger.getLogger(GenerarPrestamo.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession();
        if(session.getAttribute("logged") == null || session.getAttribute("logged").equals(0)){
            session.setAttribute("msg_response", "Acceso Denegado.");
            session.setAttribute("class_msg_response", "msg-error");
            ServletContext contexto = request.getServletContext();
            response.sendRedirect(contexto.getContextPath() + "/Logout");
            return;
        }
        
        DAOSubcategoria daoSubcategoria = new DAOSubcategoria();
        DAOSolicitudProductos daoSolicitudProds = new DAOSolicitudProductos();
        DAOSolicitud daoSolicitud = new DAOSolicitud();
        DAOProducto daoProducto = new DAOProducto();
        DAODetalleSolicitud daoDetalle = new DAODetalleSolicitud();
        
        int solicitudId = Integer.parseInt(request.getParameter("solicitudId"));
        Solicitud solicitud = null;
        try{
            solicitud = daoSolicitud.getById(solicitudId, false, false);
        }catch(Exception ex){
            logger.warning("No se pudo obtener solicitud id: "+solicitudId+". "+ex.getMessage());
            ServletContext contexto = request.getServletContext();
            response.sendRedirect(contexto.getContextPath() + "/PanoleroListadoSolicitudes");
            return;
        }
        
        String[] cantidadesPrestadasArray, idsRelacionSolicitudSubcategoriaArray,subcategoriasArray;
        cantidadesPrestadasArray = request.getParameterValues("cantidad_prestada");
        idsRelacionSolicitudSubcategoriaArray = request.getParameterValues("relaciones");
        subcategoriasArray = request.getParameterValues("subcategoriaIds");
        
        for(int i=0; i < subcategoriasArray.length; i++){
            int subcategoriaId = Integer.parseInt(subcategoriasArray[i]);
            int SolProdId = Integer.parseInt(idsRelacionSolicitudSubcategoriaArray[i]);
            int cantidadPrestada = Integer.parseInt(cantidadesPrestadasArray[i]);
            
            Subcategoria subcategoria = daoSubcategoria.getById(subcategoriaId);
            SolicitudProductos solProd = daoSolicitudProds.getById(SolProdId);
            
            // Actualizamos el stock de la subcategoría
            int cantidadSolicitada = solProd.getCantidad_solicitud();
            int stockActual = subcategoria.getStock() + cantidadSolicitada;
            int nuevoStock = stockActual - cantidadPrestada;
            subcategoria.setStock(nuevoStock);
            try{
                daoSubcategoria.edit(subcategoria);
            }catch(Exception ex){
                logger.warning("No se pudo actualizar el stock de subcategoria id: "+subcategoriaId+". "+ex.getMessage());
            }
            
            try{
                // Actualizamos la cantidad prestada de la relación entre la subcategoria con la solicitud
                solProd.setCantidad_prestada( cantidadPrestada );
                daoSolicitudProds.edit(solProd);
            }catch(Exception ex){
                logger.warning("No se pudo actualizar estado de solicitud id: "+solicitudId+". "+ex.getMessage());
            }
            // Si el producto es de tipo insumo, sólo se le restará al stock, también actualizamos el campo cantidad prestada y el estado de la solicitud.
            if(subcategoria.getTipo_producto_id() == 2){
                try{
                    // Actualizamos el estado de la solicitud
                    solicitud.setEstado_solicitud_id(3);
                    daoSolicitud.edit(solicitud);
                }catch(Exception ex){
                    logger.warning("No se pudo actualizar estado de solicitud id: "+solicitudId+". "+ex.getMessage());
                }
            
            // Si el producto es de tipo "Retornable"
            } else {
                try{
                    
                    // Obtenemos un listado de los ejemplares que habían sido solicitados de la subcategoria(producto físico en sí)
                    ArrayList<DetalleSolicitud> listadoDetalle = daoDetalle.getDetalleBySolProd(SolProdId);
                    int cant = 0;
                    
                    for(int j=0; j < listadoDetalle.size(); j++){
                        Producto producto = daoProducto.getById( listadoDetalle.get(j).getProducto_id() );
                        
                        // Cambiamos los productos solicitados a prestados.
                        if(cant < cantidadPrestada){
                            producto.setEstado_producto_id(4); //Prestado
                            
                        // Los productos que sobran los cambiamos a disponibles.
                        } else {
                            producto.setEstado_producto_id(1); //Disponible
                            try{
                                daoDetalle.deleteDetalle( listadoDetalle.get(j).getId() );
                            }catch(Exception ex){
                                logger.warning("No se pudo eliminar el detalle. "+ex.getMessage());
                            }
                        }
                        daoProducto.edit(producto);
                        cant++;
                    }
                    
                    // Actualizamos el estado de la solicitud
                    try{
                        solicitud.setEstado_solicitud_id(3);
                        daoSolicitud.edit(solicitud);
                    }catch(Exception ex){
                        logger.warning("No se pudo actualizar estado de solicitud id: "+solicitudId+". "+ex.getMessage());
                    }
                    
                }catch(Exception ex){
                    logger.warning("No se pudo obtener el listado de productos, subcategoria: "+subcategoriaId+". "+ex.getMessage());
                }
            }
        }
        ServletContext contexto = request.getServletContext();
        response.sendRedirect(contexto.getContextPath() + "/PanoleroListadoSolicitudes");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
