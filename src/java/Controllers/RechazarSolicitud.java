/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAO.DAOSolicitud;
import DAO.DAOSolicitudProductos;
import DAO.DAODetalleSolicitud;
import DAO.DAOProducto;
import DAO.DAOSubcategoria;
import Entities.Solicitud;
import Entities.SolicitudProductos;
import Entities.DetalleSolicitud;
import Entities.Producto;
import Entities.Usuario;
import Entities.Subcategoria;
import com.sun.istack.internal.logging.Logger;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Nicolas Recabarren
 */
@WebServlet(name = "RechazarSolicitud", urlPatterns = {"/RechazarSolicitud"})
public class RechazarSolicitud extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession();
        if(session.getAttribute("logged") == null || session.getAttribute("logged").equals(0)){
            session.setAttribute("msg_response", "Acceso Denegado.");
            session.setAttribute("class_msg_response", "msg-error");
            ServletContext contexto = request.getServletContext();
            response.sendRedirect(contexto.getContextPath() + "/Logout");
            return;
        }
        
        Logger logger=Logger.getLogger(RechazarSolicitud.class);
        DAOSolicitud daoSolicitud = new DAOSolicitud();
        DAOSolicitudProductos daoSolProds = new DAOSolicitudProductos();
        DAODetalleSolicitud daoDetalle = new DAODetalleSolicitud();
        DAOProducto daoProducto = new DAOProducto();
        DAOSubcategoria daoSubcategoria = new DAOSubcategoria();
        
        int solicitudId = Integer.parseInt( request.getParameter("solicitudId") );
        String observacion = request.getParameter("observacion");
        
        try{
            Solicitud solicitud = daoSolicitud.getById(solicitudId, false, false);
            solicitud.setObservacion(observacion);
            solicitud.setEstado_solicitud_id(6); // Estado: Rechazada
            solicitud.setReceptor_id( ((Usuario)session.getAttribute("user_log")).getId() );
            daoSolicitud.edit(solicitud);
            
            logger.warning("Solicitud rechazada, se continua con el cambio de estado de los productos.");
            
            // Vamos a buscar la relacion de la solicitud con las subcategorias
            ArrayList<SolicitudProductos> relaciones = new ArrayList<>();
            try{
                relaciones = daoSolProds.listarBySolicitud(solicitudId, true);
                for(int i = 0; i < relaciones.size(); i++){
                    
                    // Sólo debemos actualizar los productos que sean retornables.
                    if( relaciones.get(i).getSubcategoria().getTipo_producto_id() == 1){
                        ArrayList<DetalleSolicitud> detallesSolicitud = daoDetalle.getDetalleBySolProd(relaciones.get(i).getId());
                        
                        for(int j = 0; i < detallesSolicitud.size(); j++){
                            Producto producto = daoProducto.getById( detallesSolicitud.get(j).getProducto_id());
                            producto.setEstado_producto_id(1); // Disponible
                            try{
                                daoProducto.edit(producto);
                                logger.warning("Producto cambiado a disponible.");
                                
                            }catch(Exception ex){
                                logger.warning("No se actualizó estado de producto ID: "+detallesSolicitud.get(j).getProducto_id()+". "+ex.getMessage());
                            }
                        }
                    }
                    
                    Subcategoria subcategoria = relaciones.get(i).getSubcategoria();
                    int stockActual = subcategoria.getStock();
                    
                    subcategoria.setStock( stockActual + relaciones.get(i).getCantidad_solicitud());
                    try{
                        daoSubcategoria.edit(subcategoria);
                        logger.warning("Stock actualizado subcategoria ID: "+subcategoria.getId());
                    }catch(Exception ex){
                        logger.warning("No se pudo actualizar el stock. "+ex.getMessage());
                    }
                }
                
            }catch(Exception ex){
                logger.warning("No se recuperaron las rels. de solicitud con subcategoria. "+ex.getMessage());
            }
        }catch(Exception ex){
            logger.warning("Ha ocurrido un error al rechazar la solicitud. "+ex.getMessage());
        }
        
        ServletContext contexto = request.getServletContext();
        response.sendRedirect(contexto.getContextPath() + "/PanoleroListadoSolicitudes");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
