/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAO.DAOEstadoProducto;
import DAO.DAOProducto;
import DAO.DAOMarcoProveedor;
import DAO.DAOSubcategoria;
import Entities.EstadoProducto;
import Entities.Producto;
import Entities.MarcaProveedor;
import Entities.Subcategoria;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Nicolas Recabarren
 */
@WebServlet(name = "AdminVerDetalleProducto", urlPatterns = {"/AdminVerDetalleProducto"})
public class AdminVerDetalleProducto extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession();
        if(session.getAttribute("logged") == null || session.getAttribute("logged").equals(0)){
            session.setAttribute("msg_response", "Acceso Denegado.");
            session.setAttribute("class_msg_response", "msg-error");
            ServletContext contexto = request.getServletContext();
            response.sendRedirect(contexto.getContextPath() + "/Logout");
            return;
        }
        
        DAOEstadoProducto daoEstadoProducto = new DAOEstadoProducto();
        DAOMarcoProveedor daoMarcaProv = new DAOMarcoProveedor();
        DAOProducto daoProducto = new DAOProducto();
        DAOSubcategoria daoSubcategoria = new DAOSubcategoria();
        
        ArrayList<EstadoProducto> listadoEstados = new ArrayList<>();
        ArrayList<MarcaProveedor> listadoMarcaProveedor = new ArrayList<>();
        ArrayList<Producto> listadoProductos = new ArrayList<>();
        
        int idSubcategoria = Integer.parseInt(request.getParameter("subcategoria"));
        
        try{
            listadoEstados = daoEstadoProducto.listar();
            listadoMarcaProveedor = daoMarcaProv.listar();
            listadoProductos = daoProducto.listarBySubcategoria(idSubcategoria);
            Subcategoria subcategoria = daoSubcategoria.getById(idSubcategoria);
            
            request.setAttribute("listadoEstados", listadoEstados);
            request.setAttribute("listadoMarcaProveedor", listadoMarcaProveedor);
            request.setAttribute("listadoProductos", listadoProductos);
            request.setAttribute("subcategoria", subcategoria);
        }catch(Exception ex){}
        
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminVerDetalleProducto.jsp");
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
