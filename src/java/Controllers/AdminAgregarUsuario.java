/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

/**
 *
 * @author DuduRengo
 */
import DAO.DAOCategoria;
import DAO.DAOSede;
import DAO.DAOSubcategoria;
import DAO.DAOTipoProducto;
import DAO.DAOUsuario;
import Entities.Categoria;
import Entities.Sede;
import Entities.Subcategoria;
import Entities.TipoProducto;
import Entities.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Nicolas Recabarren
 */
@WebServlet(name = "AdminAgregarUsuario", urlPatterns = {"/AdminAgregarUsuario"})
public class AdminAgregarUsuario extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession();
        if(session.getAttribute("logged") == null || session.getAttribute("logged").equals(0)){
            session.setAttribute("msg_response", "Acceso Denegado.");
            session.setAttribute("class_msg_response", "msg-error");
            ServletContext contexto = request.getServletContext();
            response.sendRedirect(contexto.getContextPath() + "/Logout");
            return;
        }
        
        if( request.getParameter("nombre") == null){
            DAOUsuario daoUsuario = new DAOUsuario();
            DAOSede daoSede = new DAOSede();
            
            ArrayList<Usuario> listadoUsuarios = new ArrayList<>();
            ArrayList<Sede> listadoSedes = new ArrayList<>();

            try{

                listadoUsuarios = daoUsuario.listar();
                listadoSedes = daoSede.listar();
            } catch(Exception ex){}
            
            request.setAttribute("listadoUsuarios", listadoUsuarios);
            request.setAttribute("listadoSedes", listadoSedes);
            
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminAgregarUsuario.jsp");
            rd.forward(request, response);
            
        } else {
           
            ServletContext contexto = request.getServletContext();
            response.sendRedirect(contexto.getContextPath() + "/AdminListadoUsuarios");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

