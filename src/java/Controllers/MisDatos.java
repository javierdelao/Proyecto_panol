/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Entities.Usuario;
import DAO.DAOUsuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Nicolas Recabarren
 */
@WebServlet(name = "MisDatos", urlPatterns = {"/MisDatos"})
public class MisDatos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession();
        DAOUsuario daoUsuario = new DAOUsuario();
        
        session.getAttribute("user_log");
        Usuario usuario = (Usuario)session.getAttribute("user_log");
        
        if( request.getParameter("nombre") == null){
            
            request.setAttribute("usuario", usuario);
        } else {
            
            String rut = request.getParameter("rut");
            String nombre = request.getParameter("nombre");
            String apellido = request.getParameter("apellido");
            String domicilio = request.getParameter("domicilio");
            String telefono = request.getParameter("telefono");
            String correo = request.getParameter("correo");
            
            String password = usuario.getPassword();
            if(!request.getParameter("password").isEmpty()){
                password = request.getParameter("password");
            };
            
            int escuelaId = Integer.parseInt( request.getParameter("escuela_id"));
            
            usuario.setRut(rut);
            usuario.setNombre(nombre);
            usuario.setApellido(apellido);
            usuario.setDomicilio(domicilio);
            usuario.setTelefono(telefono);
            usuario.setPassword(password);
            usuario.setCorreo(correo);
            usuario.setEscuela_id(escuelaId);
            
            try{
                daoUsuario.edit(usuario);
            }catch(Exception ex){}
        }
        
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/misDatos.jsp");
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
