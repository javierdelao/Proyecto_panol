/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAO.DAOSubcategoria;
import DAO.DAOProducto;
import DAO.DAOMarcoProveedor;
import DAO.DAOEstadoProducto;
import Entities.Subcategoria;
import Entities.Producto;
import Entities.MarcaProveedor;
import Entities.EstadoProducto;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Nicolas Recabarren
 */
@WebServlet(name = "AdminEditarUnidadProducto", urlPatterns = {"/AdminEditarUnidadProducto"})
public class AdminEditarUnidadProducto extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession();
        if(session.getAttribute("logged") == null || session.getAttribute("logged").equals(0)){
            session.setAttribute("msg_response", "Acceso Denegado.");
            session.setAttribute("class_msg_response", "msg-error");
            ServletContext contexto = request.getServletContext();
            response.sendRedirect(contexto.getContextPath() + "/Logout");
            return;
        }
        
        DAOSubcategoria daoSubcategoria = new DAOSubcategoria();
        DAOProducto daoProducto = new DAOProducto();
        
        int idSubcategoria = Integer.parseInt(request.getParameter("subcategoria"));
        Subcategoria subcategoria = daoSubcategoria.getById(idSubcategoria);
        
        int idProducto = Integer.parseInt(request.getParameter("producto"));
        Producto producto = daoProducto.getById(idProducto);
        
        if( request.getParameter("nombre") == null){
            DAOEstadoProducto daoEstadoProducto = new DAOEstadoProducto();
            DAOMarcoProveedor daoMarcaProv = new DAOMarcoProveedor();
            
            ArrayList<EstadoProducto> listadoEstados = new ArrayList<>();
            ArrayList<MarcaProveedor> listadoMarcaProveedor = new ArrayList<>();
            
            try{
                listadoEstados = daoEstadoProducto.listar();
                listadoMarcaProveedor = daoMarcaProv.listar();
                
                request.setAttribute("listadoEstados", listadoEstados);
                request.setAttribute("listadoMarcaProveedor", listadoMarcaProveedor);
                request.setAttribute("subcategoria", subcategoria);
                request.setAttribute("producto", producto);
            }catch(Exception ex){}
            
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminEditarUnidadProducto.jsp");
            rd.forward(request, response);
        } else {
            String nombre = request.getParameter("nombre");
            String observacion = request.getParameter("observacion");
            int marca_proveedor_id = Integer.parseInt( request.getParameter("marca_proveedor_id") );
            int estado_producto_id = Integer.parseInt( request.getParameter("estado_producto_id") );
            
            producto.setNombre(nombre);
            producto.setObservacion(observacion);
            producto.setImagen("");
            producto.setMarca_proveedor_id(marca_proveedor_id);
            producto.setSub_categoria_id(idSubcategoria);
            producto.setEstado_producto_id(estado_producto_id);
            
            try{
                daoProducto.edit(producto);
                
                if(estado_producto_id == 3){
                    int stockActual = subcategoria.getStock();
                    int stockNuevo = stockActual-1;
                    subcategoria.setStock(stockNuevo);
                    daoSubcategoria.edit(subcategoria);
                }
                
            }catch(Exception ex){}
            
            ServletContext contexto = request.getServletContext();
            response.sendRedirect(contexto.getContextPath() + "/AdminVerDetalleProducto?subcategoria="+idSubcategoria);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
