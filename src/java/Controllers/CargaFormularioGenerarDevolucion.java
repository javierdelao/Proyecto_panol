/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Entities.Solicitud;
import Entities.SolicitudProductos;
import Entities.Producto;
import Entities.DetalleSolicitud;
import DAO.DAOSolicitud;
import DAO.DAOSolicitudProductos;
import DAO.DAOProducto;
import DAO.DAODetalleSolicitud;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Nicolas Recabarren
 */
@WebServlet(name = "CargaFormularioGenerarDevolucion", urlPatterns = {"/CargaFormularioGenerarDevolucion"})
public class CargaFormularioGenerarDevolucion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        DAOSolicitud daoSolicitud = new DAOSolicitud();
        DAODetalleSolicitud daoDetalle = new DAODetalleSolicitud();
        DAOProducto daoProducto = new DAOProducto();
        DAOSolicitudProductos daoSolProd = new DAOSolicitudProductos();
        
        int prestamoId = Integer.parseInt( request.getParameter("prestamo") );
        
        try{
            
            // Obtenemos los datos de la solicitud
            Solicitud prestamo = daoSolicitud.getById(prestamoId, true, true);
            
            ArrayList<SolicitudProductos> listadoSolProd = prestamo.getListadoProductos();
            for(int i = 0; i < listadoSolProd.size(); i++){
                
                // Obtenemos los detalles del prestamo
                int SolProdId = listadoSolProd.get(i).getId();
                ArrayList<DetalleSolicitud> detalles = daoDetalle.getDetalleBySolProd( SolProdId );
                
                for(int j=0; j < detalles.size(); j++){
                    
                    // Producto asociado al detalle
                    int productoId = detalles.get(j).getProducto_id();
                    detalles.get(j).setProducto( daoProducto.getById(productoId) );
                }
                listadoSolProd.get(i).setDetalles(detalles);
            }
            
            request.setAttribute("prestamo", prestamo);
            
        }catch(Exception ex){
            
        }
        
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/cargaFormularioGenerarDevolucion.jsp");
        rd.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
