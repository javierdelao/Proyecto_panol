/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Entities.Solicitud;
import Entities.Usuario;
import Entities.SolicitudProductos;
import Entities.Subcategoria;
import Entities.Producto;
import Entities.DetalleSolicitud;
import DAO.DAOSolicitud;
import DAO.DAOSolicitudProductos;
import DAO.DAOSubcategoria;
import DAO.DAOProducto;
import DAO.DAODetalleSolicitud;
import com.sun.istack.internal.logging.Logger;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Nicolas Recabarren
 */
@WebServlet(name = "AgregarProductoASolicitud", urlPatterns = {"/AgregarProductoASolicitud"})
public class AgregarProductoASolicitud extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        Logger logger=Logger.getLogger(AgregarProductoASolicitud.class);
        HttpSession session = request.getSession();
        
        DAOSolicitud daoSolicitud = new DAOSolicitud();
        DAOSolicitudProductos daoSolicitudProds = new DAOSolicitudProductos();
        DAOSubcategoria daoSubcategoria = new DAOSubcategoria();
        DAOProducto daoProducto = new DAOProducto();
        DAODetalleSolicitud daoDetalleSolicitud = new DAODetalleSolicitud();
        
        Usuario usuario = (Usuario)session.getAttribute("user_log");
        int subcategoriaId = Integer.parseInt( request.getParameter("producto"));
        int stock = Integer.parseInt(request.getParameter("stock"));
        String fecha_prestamo = request.getParameter("fecha_prestamo");
        
        Solicitud solicitudActiva = daoSolicitud.getSolicitudActiva(usuario.getId());
        
        if( solicitudActiva == null){
            solicitudActiva = new Solicitud();
            solicitudActiva.setUsuario_id(usuario.getId()); // Dueño de solicitud
            solicitudActiva.setTipo_solicitud_id(1); // 1: Solicitud, 2: Prestamo
            solicitudActiva.setEstado_solicitud_id(1); // Activa
            solicitudActiva.setObservacion("");
            solicitudActiva.setReceptor_id(0);
            
            Date fecha_creacion = new Date();
            solicitudActiva.setFechaCreacion(fecha_creacion);
            
            if(usuario.getPerfil_id() == 3){
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date fechaPrestamo;
                try{
                    fechaPrestamo = dateFormat.parse(fecha_prestamo.replace('/', '-'));
                    solicitudActiva.setFechaPrestamo( fechaPrestamo );
                }catch(Exception ex){
                    logger.warning("No se pudo parsear la fecha de préstamo. "+ex.getMessage());
                }
            } else {
                solicitudActiva.setFechaPrestamo( new Date() );
            }
            
            try{
                daoSolicitud.create(solicitudActiva);
                solicitudActiva = daoSolicitud.getSolicitudActiva(usuario.getId());
                logger.warning("Solicitud activa creada!.");
            }catch(Exception ex){
                logger.warning("Ha ocurrido un error al guardar la nueva solicitud. "+ex.getMessage());
            }
        }
        
        SolicitudProductos nuevoSolicitudProd = new SolicitudProductos();
        boolean existe = false;
        try{
            if(daoSolicitudProds.existeProductoEnSolicitud(solicitudActiva.getId(),subcategoriaId)){
                nuevoSolicitudProd = daoSolicitudProds.getProductoEnSolicitud(solicitudActiva.getId(), subcategoriaId);
                existe = true;
            } else {
                nuevoSolicitudProd.setSubcategoria_id(subcategoriaId);
                nuevoSolicitudProd.setSolicitud_id(solicitudActiva.getId());
            }
            nuevoSolicitudProd.setCantidad_solicitud(stock);
            
            if(!existe){
                daoSolicitudProds.create(nuevoSolicitudProd);
                logger.warning("Producto creado en la solicitud.");
            } else {
                daoSolicitudProds.edit(nuevoSolicitudProd);
                logger.warning("Producto editado en la solicitud.");
            }
            
            // Restamos la cantidad de productos solicitados del stock de la subcategoría
            Subcategoria subcategoria = daoSubcategoria.getById(subcategoriaId);
            int stockActual = subcategoria.getStock();
            subcategoria.setStock( stockActual - stock );
            
            try{
                daoSubcategoria.edit(subcategoria);
                logger.warning("Stock actualizado en la subcategoria.");
                
                // Si los productos son del tipo retornable, entonces debemos guardarlos en el detalle de la solicitud.
                // Además se deben cambiar sus estados a solicitados.
                if(subcategoria.getTipo_producto_id() == 1){
                    ArrayList<Producto> productosDisponibles = new ArrayList<>();
                    try{
                        productosDisponibles = daoProducto.listarDisponiblesBySubcategoria(subcategoriaId);
                        
                        SolicitudProductos solProd = daoSolicitudProds.getProductoEnSolicitud(solicitudActiva.getId(), subcategoriaId);
                        // Actualizamos los primeros productos disponibles encontrados a estado "solicitado".
                        for(int i = 0; i < stock; i++){
                            int productoId = productosDisponibles.get(i).getId();
                            Producto productoAActualizar = daoProducto.getById( productoId );
                            productoAActualizar.setEstado_producto_id( 5 ); // Solicitado (Reservado)
                            
                            try{
                                daoProducto.edit(productoAActualizar);
                                logger.warning("Estado de producto ID: "+productoId+" actualizado.");
                                
                                // validamos si existe ya o no el producto en el detalle de la solicitud.
                                if( !daoDetalleSolicitud.existeDetalle(nuevoSolicitudProd.getId(), productoId) ){
                                    
                                    DetalleSolicitud detalle = new DetalleSolicitud();
                                    detalle.setProducto_id(productoId);
                                    detalle.setSolicitud_productos_id(solProd.getId());
                                    
                                    try{
                                        daoDetalleSolicitud.create(detalle);
                                        logger.warning("Detalle de la solicitud creado.");
                                    }catch(Exception ex){
                                        logger.warning("Error al crear el detalle de la solicitud. "+ex.getMessage());
                                    }
                                }
                                
                            }catch(Exception ex){
                                logger.warning("Error al actualizar estado de producto ID: "+productoId+". "+ex.getMessage());
                            }

                        }

                    }catch(Exception ex){
                        logger.warning("Error al obtener productos disponibles. "+ex.getMessage());
                    }
                }
                
                
            }catch(Exception ex){
                logger.warning("Ha ocurrido un error al guardar el nuevo stock a la subcategoría." +ex.getMessage());
            }
            
        }catch(Exception ex){
            response.getWriter().write("error");
        }
        response.getWriter().write("ok");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
