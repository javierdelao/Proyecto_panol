/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAO.DAOCategoria;
import DAO.DAOSubcategoria;
import DAO.DAOTipoProducto;
import Entities.Categoria;
import Entities.Subcategoria;
import Entities.TipoProducto;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Nicolas Recabarren
 */
@WebServlet(name = "AdminAgregarProducto", urlPatterns = {"/AdminAgregarProducto"})
public class AdminAgregarProducto extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession();
        if(session.getAttribute("logged") == null || session.getAttribute("logged").equals(0)){
            session.setAttribute("msg_response", "Acceso Denegado.");
            session.setAttribute("class_msg_response", "msg-error");
            ServletContext contexto = request.getServletContext();
            response.sendRedirect(contexto.getContextPath() + "/Logout");
            return;
        }
        
        if( request.getParameter("nombre") == null){
            DAOCategoria daoCategoria = new DAOCategoria();
            DAOTipoProducto daoTipoProducto = new DAOTipoProducto();
            
            ArrayList<Categoria> listadoCategorias = new ArrayList<>();
            ArrayList<TipoProducto> listadoTipoProducto = new ArrayList<>();
            try{
                listadoCategorias = daoCategoria.listar();
                listadoTipoProducto = daoTipoProducto.listar();
            } catch(Exception ex){}
            
            request.setAttribute("listadoCategorias", listadoCategorias);
            request.setAttribute("listadoTipoProducto", listadoTipoProducto);
            
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminAgregarProducto.jsp");
            rd.forward(request, response);
            
        } else {
            DAOSubcategoria daoSubcategoria = new DAOSubcategoria();
            
            String nombre = request.getParameter("nombre");
            int categoria_id = Integer.parseInt(request.getParameter("categoria_id"));
            int tipo_producto_id = Integer.parseInt(request.getParameter("tipo_producto_id"));
            int stock = Integer.parseInt(request.getParameter("stock"));
            
            Subcategoria subcategoria = new Subcategoria();
            subcategoria.setNombre(nombre );
            subcategoria.setCategoria_id(categoria_id);
            subcategoria.setStock(stock);
            subcategoria.setTipo_producto_id(tipo_producto_id);
            
            try{
                daoSubcategoria.create(subcategoria);
            } catch(Exception ex){}
            
            ServletContext contexto = request.getServletContext();
            response.sendRedirect(contexto.getContextPath() + "/AdminListadoProductos");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
