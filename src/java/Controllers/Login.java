/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAO.DAOEscuela;
import DAO.DAOSede;
import DAO.DAOUsuario;
import Entities.Escuela;
import Entities.Sede;
import Entities.Usuario;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author javier
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        session.setAttribute("logged", 0);
            
        String action=request.getParameter("action");
        if(action==null){
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
            rd.forward(request, response);
            return;
        }
        if(action.equals("registro")){
            DAOSede daoS=new DAOSede();
            ArrayList<Sede>listaSedes= daoS.listar();
            request.setAttribute("listaSedes", listaSedes);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/registro.jsp");
            rd.forward(request, response);
            return;
        }
        if(action.equals("selectSede")){
            response.setContentType("text/json;charset=UTF-8");
            String idSede=request.getParameter("idSede");
            DAOEscuela daoE=new DAOEscuela();
            ArrayList<Escuela>listaEscuelas= daoE.listarBySedeId(Integer.parseInt(idSede));
            String json = new Gson().toJson(listaEscuelas);
            response.getWriter().write(json);
           
            return;
        }
        if(action.equals("login")){
            
            String rut=request.getParameter("rut");
            String pass=request.getParameter("password");
            DAOUsuario daoU=new DAOUsuario();
            Usuario us=daoU.getByRut(rut);
            
            if(us==null){
                session.setAttribute("msg_response", "Rut ingresado no existe.");
                session.setAttribute("class_msg_response", "msg-error");
                session.setAttribute("logged", 0);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
                rd.forward(request, response);
                return;
            }
            if(us.getPassword().equals(pass)){
                
                session.setAttribute("user_log", us);
                session.setAttribute("usuario_perfil", us.getPerfil_id());
                session.setAttribute("nombreUserLog", us.getNombre()+" "+us.getApellido());
                session.setAttribute("logged", 1);
                
                ServletContext contexto = request.getServletContext();
                switch(us.getPerfil_id()){
                    case 1: // Pañolero
                        response.sendRedirect(contexto.getContextPath() + "/PanoleroListadoSolicitudes");
                        break;
                    case 2: //Alumno
                    case 3: //Profesor
                    case 4: //Invitado
                        response.sendRedirect(contexto.getContextPath() + "/CatalogoProductos");
                        break;
                    case 6:
                        response.sendRedirect(contexto.getContextPath() + "/AdminListadoProductos");
                        break;
                }
                return;
          
            }else{
                session.setAttribute("msg_response", "Rut o contraseña ingresados es/son incorrectos.");
                session.setAttribute("class_msg_response", "msg-error");
                session.setAttribute("logged", 0);
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
                rd.forward(request, response);
                return;
            }
        }
        if(action.equals("register")){
            DAOUsuario daoU=new DAOUsuario();
            String rut=request.getParameter("rut");
            String nombre=request.getParameter("nombre");
            String apellido=request.getParameter("apellido");
            String domicilio=request.getParameter("domicilio");
            String telefono=request.getParameter("telefono");
            String correo=request.getParameter("correo");
            int escuela=Integer.parseInt(request.getParameter("escuela"));
            int tipo_usuario=Integer.parseInt(request.getParameter("tipo_usuario"));
            
            Usuario us=new Usuario();
            us.setRut(rut);
            us.setNombre(nombre);
            us.setApellido(apellido);
            us.setDomicilio(domicilio);
            us.setTelefono(telefono);
            us.setCorreo(correo);
            us.setPassword("123");
            us.setEscuela_id(escuela);
            us.setPerfil_id(escuela);
            try {
                daoU.create(us);
            } catch (SQLException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
            rd.forward(request, response);
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
