/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author javier
 */
public class Escuela {
    private int id;
    private String nombre;
    private int sede_id;

    public Escuela() {
    }

    public Escuela(int id, String nombre, int sede_id) {
        this.id = id;
        this.nombre = nombre;
        this.sede_id = sede_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSede_id() {
        return sede_id;
    }

    public void setSede_id(int sede_id) {
        this.sede_id = sede_id;
    }
    
    
}
