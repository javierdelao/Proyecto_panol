/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.ArrayList;
import java.util.Date;
/**
 *
 * @author javier
 */
public class Solicitud {
    private int id;
    private int usuario_id;
    private int tipo_solicitud_id;
    private int estado_solicitud_id;
    private String observacion;
    private int receptor_id;
    private Date fecha_creacion;
    private Date fecha_prestamo;
    private ArrayList<SolicitudProductos> listadoProductos;
    private Usuario usuario;

    public Solicitud() {
    }

    public Solicitud(int id, int usuario_id, int tipo_solicitud_id, int estado_solicitud_id, String observacion, int receptor_id) {
        this.id = id;
        this.usuario_id = usuario_id;
        this.tipo_solicitud_id = tipo_solicitud_id;
        this.estado_solicitud_id = estado_solicitud_id;
        this.observacion = observacion;
        this.receptor_id = receptor_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public int getTipo_solicitud_id() {
        return tipo_solicitud_id;
    }

    public void setTipo_solicitud_id(int tipo_solicitud_id) {
        this.tipo_solicitud_id = tipo_solicitud_id;
    }

    public int getEstado_solicitud_id() {
        return estado_solicitud_id;
    }

    public void setEstado_solicitud_id(int estado_solicitud_id) {
        this.estado_solicitud_id = estado_solicitud_id;
    }
    
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
    public int getReceptor_id() {
        return receptor_id;
    }

    public void setReceptor_id(int receptor_id) {
        this.receptor_id = receptor_id;
    }
    
    public Date getFecha_creacion(){
        return fecha_creacion;
    }
    
    public void setFechaCreacion(Date fecha_creacion){
        this.fecha_creacion = fecha_creacion;
    }
    
    public Date getFecha_prestamo(){
        return fecha_prestamo;
    }
    
    public void setFechaPrestamo(Date fecha_prestamo){
        this.fecha_prestamo = fecha_prestamo;
    }
    
    public ArrayList<SolicitudProductos> getListadoProductos(){
        return listadoProductos;
    }
    
    public void setListadoSubcategorias(ArrayList<SolicitudProductos> listado){
        this.listadoProductos = listado;
    }
    
    public Usuario getUsuario(){
        return usuario;
    }
    
    public void setUsuario(Usuario usuario){
        this.usuario = usuario;
    }
}
