/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class SolicitudProductos {
    private int id;
    private int cantidad_solicitud;
    private int cantidad_prestada;
    private int solicitud_id;
    private int subcategoria_id;
    private Subcategoria subcategoria;
    private ArrayList<DetalleSolicitud> detalles;
    
    public SolicitudProductos() {
    }

    public SolicitudProductos(int id, int cantidad_solicitud, int solicitud_id, int subcategoria_id) {
        this.id = id;
        this.cantidad_solicitud = cantidad_solicitud;
        this.solicitud_id = solicitud_id;
        this.subcategoria_id = subcategoria_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCantidad_solicitud() {
        return cantidad_solicitud;
    }

    public void setCantidad_solicitud(int cantidad_solicitud) {
        this.cantidad_solicitud = cantidad_solicitud;
    }
    
    public int getCantidad_prestada(){
        return cantidad_prestada;
    }
    
    public void setCantidad_prestada(int cantidad_prestada){
        this.cantidad_prestada = cantidad_prestada;
    }

    public int getSolicitud_id() {
        return solicitud_id;
    }

    public void setSolicitud_id(int solicitud_id) {
        this.solicitud_id = solicitud_id;
    }

    public int getSubcategoria_id() {
        return subcategoria_id;
    }

    public void setSubcategoria_id(int subcategoria_id) {
        this.subcategoria_id = subcategoria_id;
    }
    
    public Subcategoria getSubcategoria(){
        return subcategoria;
    }
    
    public void setSubcategoria(Subcategoria subcategoria){
        this.subcategoria = subcategoria;
    }

    public ArrayList<DetalleSolicitud> getDetalles() {
        return detalles;
    }
    
    public void setDetalles(ArrayList<DetalleSolicitud> detalles){
        this.detalles = detalles;
    }
    
}
