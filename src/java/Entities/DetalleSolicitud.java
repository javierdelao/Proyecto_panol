/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;


public class DetalleSolicitud{
    private int id;
    private int solicitud_productos_id;
    private int producto_id;
    private Producto producto;
    
    public DetalleSolicitud(){}
    
    public DetalleSolicitud(int id, int solicitud_productos_id, int producto_id) {
        this.id = id;
        this.solicitud_productos_id = solicitud_productos_id;
        this.producto_id = producto_id;
    }

    public int getId() {
        return id;
    }

    public int getSolicitud_productos_id() {
        return solicitud_productos_id;
    }

    public int getProducto_id() {
        return producto_id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSolicitud_productos_id(int solicitud_productos_id) {
        this.solicitud_productos_id = solicitud_productos_id;
    }

    public void setProducto_id(int producto_id) {
        this.producto_id = producto_id;
    }
    
    public Producto getProducto(){
        return producto;
    }
    
    public void setProducto(Producto producto){
        this.producto = producto;
    }
    
}
