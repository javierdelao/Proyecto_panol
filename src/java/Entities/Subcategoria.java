/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author javier
 */
public class Subcategoria {
    private int id;
    private String nombre;
    private int stock;
    private int categoria_id;
    private int tipo_producto_id;

    public Subcategoria() {
    }

    public Subcategoria(int id, String nombre, int stock, int categoria_id, int tipo_producto_id) {
        this.id = id;
        this.nombre = nombre;
        this.stock = stock;
        this.categoria_id = categoria_id;
        this.tipo_producto_id = tipo_producto_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getCategoria_id() {
        return categoria_id;
    }

    public void setCategoria_id(int categoria_id) {
        this.categoria_id = categoria_id;
    }
    
    public int getTipo_producto_id() {
        return tipo_producto_id;
    }

    public void setTipo_producto_id(int tipo_producto_id) {
        this.tipo_producto_id = tipo_producto_id;
    }
}
