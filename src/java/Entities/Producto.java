/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author javier
 */
public class Producto {
    private int id;
    private String nombre;
    private String observacion;
    private String imagen;
    private int marca_proveedor_id;
    private int sub_categoria_id;
    private int estado_producto_id;

    public Producto() {
    }

    public Producto(int id, String nombre, String observacion, String imagen, int marca_proveedor_id, int sub_categoria_id, int estado_producto_id) {
        this.id = id;
        this.nombre = nombre;
        this.observacion = observacion;
        this.imagen = imagen;
        this.marca_proveedor_id = marca_proveedor_id;
        this.sub_categoria_id = sub_categoria_id;
        this.estado_producto_id = estado_producto_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public int getMarca_proveedor_id() {
        return marca_proveedor_id;
    }

    public void setMarca_proveedor_id(int marca_proveedor_id) {
        this.marca_proveedor_id = marca_proveedor_id;
    }

    public int getSub_categoria_id() {
        return sub_categoria_id;
    }

    public void setSub_categoria_id(int sub_categoria_id) {
        this.sub_categoria_id = sub_categoria_id;
    }

    public int getEstado_producto_id() {
        return estado_producto_id;
    }

    public void setEstado_producto_id(int estado_producto_id) {
        this.estado_producto_id = estado_producto_id;
    }
    
    

}
