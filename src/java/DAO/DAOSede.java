/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Producto;
import Entities.Sede;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOSede {
    
    Logger logger=Logger.getLogger(Conexion.class);
    
        public ArrayList<Sede> listar(){
           Conexion con=new Conexion();
          ArrayList<Sede>lista=new ArrayList<>();
        String query="select * from Sede";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
             
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Sede sede=new Sede();
                sede.setId(rs.getInt("id"));
                sede.setNombre(rs.getString("nombre"));
               
                lista.add(sede);
             }
            con.getConnection().close();  
        } catch (Exception e) {
           logger.warning("error al recuperar lista de sedes "+e.getMessage());
           return lista;
        }
         logger.info("listarSedes ok ");
        return lista;
    }
        
        
        public Sede getById(int id){
           Conexion con=new Conexion();
         Sede sede=null;
        String query="select * from Sede where id=?";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
              statement.setInt(1, id);
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                sede=new Sede();
                sede.setId(rs.getInt("id"));
                sede.setNombre(rs.getString("nombre"));
             }
            con.getConnection().close(); 
        } catch (Exception e) {
           logger.warning("error al recuperar lista de sedes "+e.getMessage());
           return sede;
        }
        logger.info("getSedeById ok ");
        return sede;
    }
        
        
             public String create(Sede sede) throws SQLException {
         Conexion con=new Conexion();
      String query="INSERT INTO sede(nombre)"
              + " values (?)";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setString(1, sede.getNombre());
            statement.executeUpdate();
            con.getConnection().close();
            
        } catch (SQLException ex) {
            logger.warning("error al crear sede "+ex.toString()+" "+ex.getMessage());
            return ex.toString()+" "+ex.getMessage();
        }
         logger.warning("sede creada");
        return "ok";
    }
             
             
          public String delete(int id) throws SQLException {
           Conexion con=new Conexion();
         String query ="DELETE FROM Sede WHERE id = ?";
         
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
            con.getConnection().close();
        } catch (SQLException ex) {
          logger.warning("error al eliminar sede"+ex.getMessage());
          return ex.getMessage();
        }
         
        logger.warning("sede eliminada ok");
        return "ok";
    }
          
     public String edit(Sede sede) throws SQLException {
        Conexion con=new Conexion();

         String query="UPDATE sede SET"
                + " nombre = ?"   
                + " WHERE id = ? ;";

       try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setString(1, sede.getNombre());    
            statement.setInt(2, sede.getId()); 
            statement.executeUpdate();
            
            con.getConnection().close();
        } catch (SQLException ex) {
          logger.warning("error al acutalizar sede "+ex.getMessage());
          return ex.getMessage();
        }
     logger.warning("sede Actualizada ok");
     return "ok";
    }
                
                

        
        
}
