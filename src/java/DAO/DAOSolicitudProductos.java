/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.SolicitudProductos;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOSolicitudProductos {
    Logger logger=Logger.getLogger(Conexion.class);
    
    public ArrayList<SolicitudProductos> listar(){
        Conexion con=new Conexion();
        ArrayList<SolicitudProductos>lista=new ArrayList<>();
        String query="SELECT * FROM solicitud_productos";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                SolicitudProductos solprod=new SolicitudProductos();
                solprod.setId(rs.getInt("id"));
                solprod.setCantidad_solicitud(rs.getInt("cantidad_solicitada"));
                solprod.setCantidad_prestada(rs.getInt("cantidad_prestada"));
                solprod.setSolicitud_id(rs.getInt("solicitud_id"));
                solprod.setSubcategoria_id(rs.getInt("subcategoria_id"));
                
                lista.add(solprod);
             }
            con.getConnection().close();  
        } catch (Exception e) {
           logger.warning("error al recuperar lista de solicitud producto "+e.getMessage());
           return lista;
        }
        logger.info("listarsolicitud producto ok ");
        return lista;
    }
    
    public ArrayList<SolicitudProductos> listarBySolicitud(int solicitud_id, boolean conProducto){
        Conexion con=new Conexion();
        ArrayList<SolicitudProductos>lista=new ArrayList<>();
        String query="SELECT * FROM solicitud_productos WHERE solicitud_id = ?";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, solicitud_id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                SolicitudProductos solprod=new SolicitudProductos();
                solprod.setId(rs.getInt("id"));
                solprod.setCantidad_solicitud(rs.getInt("cantidad_solicitada"));
                solprod.setCantidad_prestada(rs.getInt("cantidad_prestada"));
                solprod.setSolicitud_id(rs.getInt("solicitud_id"));
                solprod.setSubcategoria_id(rs.getInt("subcategoria_id"));
                
                if(conProducto){
                    DAOSubcategoria daoSubcategoria = new DAOSubcategoria();
                    solprod.setSubcategoria(
                        daoSubcategoria.getById(solprod.getSubcategoria_id())
                    );
                }
                lista.add(solprod);
             }
            con.getConnection().close();  
        } catch (Exception e) {
           logger.warning("Error al recuperar lista de solicitud productos por solicitud "+e.getMessage());
           return lista;
        }
        logger.info("listar solicitud producto por solicitud ok ");
        return lista;
    }
    
    public SolicitudProductos getById(int id){
        Conexion con=new Conexion();
        SolicitudProductos solProd=null;
        String query="select * from solicitud_productos where id=?";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                solProd=new SolicitudProductos();
                solProd.setId(rs.getInt("id"));
                solProd.setCantidad_solicitud(rs.getInt("cantidad_solicitada"));
                solProd.setCantidad_prestada(rs.getInt("cantidad_prestada"));
                solProd.setSolicitud_id(rs.getInt("solicitud_id"));
                solProd.setSubcategoria_id(rs.getInt("subcategoria_id"));
                
            }
            con.getConnection().close();
            
        } catch (Exception e) {
           logger.warning("error al recuperar lista de solicitud_productos "+e.getMessage());
           return solProd;
        }
        logger.info("getSolicitudProductoById solicitud_productos ok ");
        return solProd;
    }
    
    public boolean existeProductoEnSolicitud(int solicitud_id, int subcategoria_id){
        Conexion con=new Conexion();
        
        String query="SELECT * FROM solicitud_productos WHERE solicitud_id = ? AND subcategoria_id = ?";
        int cant = 0;
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, solicitud_id);
            statement.setInt(2, subcategoria_id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                cant++;
            }
            con.getConnection().close();
            
        } catch (Exception e) {
            logger.warning("Error al recuperar relacion de solicitud con producto. "+e.getMessage());
            return true;
        }
        
        logger.info("solicitud_productos ok ");
        return cant > 0 ? true: false;
    }
    
    public SolicitudProductos getProductoEnSolicitud(int solicitud_id, int subcategoria_id){
        Conexion con=new Conexion();
        SolicitudProductos solProd = null;
        
        String query="SELECT * FROM solicitud_productos WHERE solicitud_id = ? AND subcategoria_id = ?";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, solicitud_id);
            statement.setInt(2, subcategoria_id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                solProd = new SolicitudProductos();
                solProd.setId(rs.getInt("id"));
                solProd.setCantidad_solicitud(rs.getInt("cantidad_solicitada"));
                solProd.setCantidad_prestada(rs.getInt("cantidad_prestada"));
                solProd.setSolicitud_id(rs.getInt("solicitud_id"));
                solProd.setSubcategoria_id(rs.getInt("subcategoria_id"));
            }
            con.getConnection().close();
            
        } catch (Exception e) {
            logger.warning("Error al recuperar relacion de solicitud con producto. "+e.getMessage());
            return solProd;
        }
        
        logger.info("solicitud_productos ok ");
        return solProd;
    }
    
    public String create(SolicitudProductos solProd) throws SQLException {
            
        Conexion con=new Conexion();
        String query="INSERT INTO solicitud_productos(cantidad_solicitada,solicitud_id,subcategoria_id)"
              + " values (?,?,?)";
        try { 
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, solProd.getCantidad_solicitud());
            statement.setInt(2, solProd.getSolicitud_id());
            statement.setInt(3, solProd.getSubcategoria_id());
            statement.executeUpdate();
            con.getConnection().close();

        } catch (SQLException ex) {
            logger.warning("error al crear SolicitudProductos "+ex.toString()+" "+ex.getMessage());
            return ex.toString()+" "+ex.getMessage();
        }
        logger.warning("SolicitudProductos creada");
        
        return "ok";
    }
    
    public String edit(SolicitudProductos solProd) throws SQLException {
        Conexion con=new Conexion();
        
        String query="UPDATE solicitud_productos SET"
                + " cantidad_solicitada = ?,"
                + " cantidad_prestada = ?,"
                + " solicitud_id = ?,"
                + " subcategoria_id = ?"
                + " WHERE id = ? ;";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, solProd.getCantidad_solicitud());
            statement.setInt(2, solProd.getCantidad_prestada());
            statement.setInt(3, solProd.getSolicitud_id());
            statement.setInt(4, solProd.getSubcategoria_id());
            statement.setInt(5, solProd.getId());
            statement.executeUpdate();
            
            con.getConnection().close();
        } catch (SQLException ex) {
            logger.warning("error al actualizar SolicitudProductos "+ex.getMessage());
            return ex.getMessage();
        }
        logger.warning("SolicitudProductos Actualizada ok");
        return "ok";
    }
    
}
