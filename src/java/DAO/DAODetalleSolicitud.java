/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.DetalleSolicitud;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Nicolas Recabarren
 */
public class DAODetalleSolicitud {
    Logger logger=Logger.getLogger(Conexion.class);
    
    public ArrayList<DetalleSolicitud> listar(){
        Conexion con=new Conexion();
        ArrayList<DetalleSolicitud> lista = new ArrayList<>();
        
        String query="SELECT * FROM detalle_solicitud";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                DetalleSolicitud detalle = new DetalleSolicitud();
                detalle.setId(rs.getInt("id"));
                detalle.setProducto_id(rs.getInt("producto_id"));
                detalle.setSolicitud_productos_id(rs.getInt("solicitud_productos_id"));
                
                lista.add(detalle);
            }
            con.getConnection().close();
            
        } catch (Exception e) {
            logger.warning("Error al recuperar lista de detalles. "+e.getMessage());
            return lista;
        }
        logger.info("listar detalle ok ");
        return lista;
    }
    
    public DetalleSolicitud getById(int id){
        Conexion con=new Conexion();
        DetalleSolicitud detalle = null;
        
        String query="SELECT * FROM detalle_solicitud WHERE id=?";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                detalle = new DetalleSolicitud();
                detalle.setId(rs.getInt("id"));
                detalle.setProducto_id(rs.getInt("producto_id"));
                detalle.setSolicitud_productos_id(rs.getInt("solicitud_productos_id"));
            }
            con.getConnection().close();
            
        } catch (Exception e) {
           logger.warning("error al recuperar lista de solicitud "+e.getMessage());
           return detalle;
        }
        logger.info("getSolicitudById solicitud ok ");
        return detalle;
    }
    
    public String create(DetalleSolicitud detalle) throws SQLException {
        Conexion con=new Conexion();
        String query="INSERT INTO detalle_solicitud(producto_id,solicitud_productos_id)"
            + " values (?,?)";
        try {
            
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, detalle.getProducto_id());
            statement.setInt(2, detalle.getSolicitud_productos_id());
            statement.executeUpdate();
            
            con.getConnection().close();
            
        } catch (SQLException ex) {
            logger.warning("error al crear detalle "+ex.toString()+" "+ex.getMessage());
            return ex.toString()+" "+ex.getMessage();
        }
        logger.warning("detalle creado");
        return "ok";
    }
    
    public String edit(DetalleSolicitud detalle) throws SQLException {
        Conexion con=new Conexion();
        String query="UPDATE detalle_solicitud SET"
                + " producto_id = ?,"
                + " WHERE id = ? ;";
        
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, detalle.getProducto_id());
            statement.setInt(2, detalle.getId());
            statement.executeUpdate();
            
            con.getConnection().close();
        } catch (SQLException ex) {
          logger.warning("error al actualizar detalle "+ex.getMessage());
          return ex.getMessage();
        }
        logger.warning("detalle actualizado ok");
        return "ok";
    }
    
    public boolean existeDetalle(int solicitudProdId, int productoId){
        Conexion con = new Conexion();
        String query = "SELECT id FROM detalle_solicitud WHERE solicitud_productos_id = ? AND producto_id = ?";
        
        int count = 0;
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, solicitudProdId);
            statement.setInt(2, productoId);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                
                count++;
            }
            con.getConnection().close();
            
        } catch (Exception e) {
           logger.warning("error al recuperar detalle "+e.getMessage());
           return true;
        }
        logger.info("detalle ok.");
        return count > 0 ? true : false;
    }
    
    public ArrayList<DetalleSolicitud> getDetalleBySolProd(int solicitudProdId){
        Conexion con = new Conexion();
        String query = "SELECT * FROM detalle_solicitud WHERE solicitud_productos_id = ?";
        
        ArrayList<DetalleSolicitud> detalles = new ArrayList<>();
        try{
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, solicitudProdId);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                DetalleSolicitud detalle = new DetalleSolicitud();
                detalle.setId(rs.getInt("id"));
                detalle.setProducto_id(rs.getInt("producto_id"));
                detalle.setSolicitud_productos_id(rs.getInt("solicitud_productos_id"));
                
                detalles.add(detalle);
            }
            con.getConnection().close();
        }catch(Exception ex){
            logger.info("No se pudo recuperar el detalle.");
        }
        return detalles;
    }
    
    public boolean deleteDetalle(int idDetalle){
        Conexion con = new Conexion();
        String query = "DELETE FROM detalle_solicitud WHERE id = ?";
        
        try{
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, idDetalle);
            statement.execute();
            con.getConnection().close();
            
        }catch(Exception ex){
            logger.info("No se pudo eliminar el detalle. "+ex.getMessage());
            return false;
        }
        return true;
    }
}
