/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.TipoSolicitud;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOTipoSolicitud {
        Logger logger=Logger.getLogger(Conexion.class);
    
        public ArrayList<TipoSolicitud> listar(){
           Conexion con=new Conexion();
          ArrayList<TipoSolicitud>lista=new ArrayList<>();
        String query="select * from tipo_solicitud ";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
             
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                TipoSolicitud tipoSol=new TipoSolicitud();
                tipoSol.setId(rs.getInt("id"));
                tipoSol.setDescripcion(rs.getString("nombre"));
               
                lista.add(tipoSol);

             }
            
            con.getConnection().close();
        } catch (Exception e) {
           logger.warning("error al recuperar lista de tipo_solicitud "+e.getMessage());
           return lista;
        }
         logger.info("listar tipo_solicitud ok ");
        return lista;
    }
        
        
                public TipoSolicitud getById(int id){
           Conexion con=new Conexion();
         TipoSolicitud tipoSol=null;
        String query="select * from  tipo_solicitud where  id=?";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
              statement.setInt(1, id);
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                tipoSol=new TipoSolicitud();
                tipoSol.setId(rs.getInt("id"));
                tipoSol.setDescripcion(rs.getString("descripcion"));
             }
            
            con.getConnection().close();
             
             
             
        } catch (Exception e) {
           logger.warning("error al recuperar lista de tipos de solicitud "+e.getMessage());
           return tipoSol;
        }
        logger.info("getSedeById tipo_solicitud ok ");
        return tipoSol;
    }
}
