/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Categoria;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOCategoria {
    Logger logger=Logger.getLogger(Conexion.class);
    
        public ArrayList<Categoria> listar(){
           Conexion con=new Conexion();
          ArrayList<Categoria>lista=new ArrayList<>();
        String query="select * from categoria";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
             
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Categoria cate=new Categoria();
                cate.setId(rs.getInt("id"));
                cate.setNombre(rs.getString("nombre"));
               
                lista.add(cate);
             }
            con.getConnection().close();  
        } catch (Exception e) {
           logger.warning("error al recuperar lista de categoria "+e.getMessage());
           return lista;
        }
         logger.info("listar categoria ok ");
        return lista;
    }
        
       public Categoria getById(int id){
           Conexion con=new Conexion();
         Categoria cate=null;
        String query="select * from categoria where id=?";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
              statement.setInt(1, id);
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                cate=new Categoria();
                cate.setId(rs.getInt("id"));
                cate.setNombre(rs.getString("nombre"));
             }
            con.getConnection().close(); 
        } catch (Exception e) {
           logger.warning("error al recuperar lista de categoria "+e.getMessage());
           return cate;
        }
        logger.info("getSedeById categoria ok ");
        return cate;
    }
       
       
    public String create(Categoria cat) throws SQLException {
        Conexion con=new Conexion();
        String query="INSERT INTO categoria(nombre)" + " values (?)";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setString(1, cat.getNombre());
            statement.executeUpdate();
            con.getConnection().close();
            
        } catch (SQLException ex) {
            logger.warning("error al crear Categoria "+ex.toString()+" "+ex.getMessage());
            return ex.toString()+" "+ex.getMessage();
        }
        logger.warning("Categoria creada");
        return "ok";
    }
    
    public String edit(Categoria cat) throws SQLException {
        Conexion con=new Conexion();

         String query="UPDATE categoria SET"
                + " nombre = ?"   
                + " WHERE id = ? ;";

       try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setString(1, cat.getNombre());    
            statement.setInt(2, cat.getId()); 
            statement.executeUpdate();
            
            con.getConnection().close();
        } catch (SQLException ex) {
          logger.warning("error al acutalizar categoria "+ex.getMessage());
          return ex.getMessage();
        }
     logger.warning("categoria Actualizada ok");
     return "ok";
    }
}
