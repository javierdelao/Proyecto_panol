/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Producto;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOProducto {
    Logger logger=Logger.getLogger(Conexion.class);
    
    public ArrayList<Producto> listar(){
        Conexion con=new Conexion();
        ArrayList<Producto>lista=new ArrayList<>();
        String query="select * from producto";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
             
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Producto prod=new Producto();
                prod.setId(rs.getInt("id"));
                prod.setNombre(rs.getString("nombre"));
                prod.setObservacion(rs.getString("observacion"));
                prod.setImagen(rs.getString("imagen"));
                prod.setMarca_proveedor_id(rs.getInt("marca_proveedor_id"));
                prod.setSub_categoria_id(rs.getInt("subcategoria_id"));
                prod.setEstado_producto_id(rs.getInt("estado_producto_id"));
            
                lista.add(prod);

            }
            
            con.getConnection().close();
             
            
        } catch (Exception e) {
           logger.warning("error al recuperar lista de Producto "+e.getMessage());
           return lista;
        }
        logger.info("listar Producto ok ");
        return lista;
    }
    
    public boolean validaProductosDisponibles(int subcategoriaId, int cantidadAPrestar){
        Conexion con=new Conexion();
        
        int cantidadDisponibles = 0;
        String query = "SELECT id FROM producto WHERE Subcategoria_id = ? AND estado_producto_id = 1";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, subcategoriaId);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                cantidadDisponibles++;
            }
            con.getConnection().close();
            
        } catch (Exception e) {
           logger.warning("error al recuperar lista de Producto "+e.getMessage());
           return false;
        }
        logger.info("listar Producto ok ");
        return cantidadDisponibles >= cantidadAPrestar ? true : false;
    }
    
    public ArrayList<Producto> listarBySubcategoria(int subcategoriaId){
        Conexion con=new Conexion();
        ArrayList<Producto>lista=new ArrayList<>();
        String query = "SELECT * FROM producto WHERE Subcategoria_id = ?";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, subcategoriaId);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Producto prod=new Producto();
                prod.setId(rs.getInt("id"));
                prod.setNombre(rs.getString("nombre"));
                prod.setObservacion(rs.getString("observacion"));
                prod.setImagen(rs.getString("imagen"));
                prod.setMarca_proveedor_id(rs.getInt("marca_proveedor_id"));
                prod.setSub_categoria_id(rs.getInt("subcategoria_id"));
                prod.setEstado_producto_id(rs.getInt("estado_producto_id"));
                
                lista.add(prod);
            }
            con.getConnection().close();
             
            
        } catch (Exception e) {
           logger.warning("error al recuperar lista de Producto "+e.getMessage());
           return lista;
        }
        logger.info("listar Producto ok ");
        return lista;
    }
    
    public ArrayList<Producto> listarDisponiblesBySubcategoria(int subcategoriaId){
        Conexion con=new Conexion();
        ArrayList<Producto>lista=new ArrayList<>();
        String query = "SELECT * FROM producto WHERE Subcategoria_id = ? AND Estado_Producto_id = 1";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, subcategoriaId);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Producto prod=new Producto();
                prod.setId(rs.getInt("id"));
                prod.setNombre(rs.getString("nombre"));
                prod.setObservacion(rs.getString("observacion"));
                prod.setImagen(rs.getString("imagen"));
                prod.setMarca_proveedor_id(rs.getInt("marca_proveedor_id"));
                prod.setSub_categoria_id(rs.getInt("subcategoria_id"));
                prod.setEstado_producto_id(rs.getInt("estado_producto_id"));
                
                lista.add(prod);
            }
            con.getConnection().close();
             
            
        } catch (Exception e) {
           logger.warning("error al recuperar lista de Producto "+e.getMessage());
           return lista;
        }
        logger.info("listar Producto ok ");
        return lista;
    }
       
    public Producto getById(int id){
        Conexion con=new Conexion();
        Producto prod=null;
        String query="select * from producto where id=?";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                prod=new Producto();
                prod.setId(rs.getInt("id"));
                prod.setNombre(rs.getString("nombre"));
                prod.setObservacion(rs.getString("observacion"));
                prod.setImagen(rs.getString("imagen"));
                prod.setMarca_proveedor_id(rs.getInt("marca_proveedor_id"));
                prod.setSub_categoria_id(rs.getInt("subcategoria_id"));
                prod.setEstado_producto_id(rs.getInt("estado_producto_id"));
            }
            
            con.getConnection().close();
             
             
             
        } catch (Exception e) {
           logger.warning("error al recuperar Producto "+e.getMessage());
           return prod;
        }
        logger.info("getSedeById Producto ok ");
        return prod;
    }
        
    public String create(Producto prod) throws SQLException {
        Conexion con=new Conexion();
        String query="INSERT INTO producto(nombre,observacion,imagen,marca_proveedor_id,subcategoria_id,estado_producto_id)"
            + " values (?,?,?,?,?,?)";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setString(1, prod.getNombre());
            statement.setString(2, prod.getObservacion());
            statement.setString(3, prod.getImagen());
            statement.setInt(4, prod.getMarca_proveedor_id());
            statement.setInt(5, prod.getSub_categoria_id());
            statement.setInt(6, prod.getEstado_producto_id());
            statement.executeUpdate();
            
            con.getConnection().close();
            
        } catch (SQLException ex) {
            logger.warning("error al crear Producto "+ex.toString()+" "+ex.getMessage());
            return ex.toString()+" "+ex.getMessage();
        }
        logger.warning("Producto creada");
        return "ok";
        
        
    }
    
    public String edit(Producto prod) throws SQLException {
        Conexion con=new Conexion();
      
        String query="UPDATE producto SET"
            + " nombre = ?," 
            + " observacion = ?," 
            + " imagen = ?," 
            + " marca_proveedor_id = ?," 
            + " subcategoria_id = ?," 
            + " estado_producto_id = ?"  
            + " WHERE id = ? ;";
        
        
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setString(1, prod.getNombre());
            statement.setString(2, prod.getObservacion());
            statement.setString(3, prod.getImagen());
            statement.setInt(4, prod.getMarca_proveedor_id());
            statement.setInt(5, prod.getSub_categoria_id());
            statement.setInt(6, prod.getEstado_producto_id());
            statement.setInt(7, prod.getId());
         
            statement.executeUpdate();
            
            con.getConnection().close();
        } catch (SQLException ex) {
          logger.warning("Error al actualizar Producto "+ex.getMessage());
          return ex.getMessage();
        }
        logger.warning("Producto Actualizada ok");
        return "ok";

    }
}
