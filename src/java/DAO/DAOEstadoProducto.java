/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.EstadoProducto;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOEstadoProducto {
       Logger logger=Logger.getLogger(Conexion.class);
    
        public ArrayList<EstadoProducto> listar(){
           Conexion con=new Conexion();
          ArrayList<EstadoProducto>lista=new ArrayList<>();
        String query="select * from estado_producto";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
             
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                EstadoProducto estadoProd=new EstadoProducto();
                estadoProd.setId(rs.getInt("id"));
                estadoProd.setDescripcion(rs.getString("descripcion"));
               
                lista.add(estadoProd);
             }
            con.getConnection().close();  
        } catch (Exception e) {
           logger.warning("error al recuperar lista de EstadoProducto "+e.getMessage());
           return lista;
        }
         logger.info("listar EstadoProducto ok ");
        return lista;
    }
        
        
        public EstadoProducto getById(int id){
           Conexion con=new Conexion();
         EstadoProducto estadoProd=null;
        String query="select * from estado_producto where id=?";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
              statement.setInt(1, id);
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                estadoProd=new EstadoProducto();
                estadoProd.setId(rs.getInt("id"));
                estadoProd.setDescripcion(rs.getString("descripcion"));
             }
            con.getConnection().close(); 
        } catch (Exception e) {
           logger.warning("error al recuperar lista de EstadoProducto "+e.getMessage());
           return estadoProd;
        }
        logger.info("getSedeById EstadoProducto ok ");
        return estadoProd;
    }
}
