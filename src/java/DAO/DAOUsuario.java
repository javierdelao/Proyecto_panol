/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Usuario;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOUsuario {
    Logger logger=Logger.getLogger(Conexion.class);
    
            public ArrayList<Usuario> listar(){
           Conexion con=new Conexion();
          ArrayList<Usuario>lista=new ArrayList<>();
        String query="select * from usuario";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
             
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Usuario usuario=new Usuario();
                usuario.setId(rs.getInt("id"));
                usuario.setRut(rs.getString("rut"));
                usuario.setNombre(rs.getString("nombre"));
                usuario.setApellido(rs.getString("apellido"));
                usuario.setDomicilio(rs.getString("domicilio"));
                usuario.setTelefono(rs.getString("telefono"));
                usuario.setCorreo(rs.getString("correo"));
                usuario.setPassword(rs.getString("password"));
                usuario.setEscuela_id(rs.getInt("escuela_id"));
                usuario.setPerfil_id(rs.getInt("perfil_id"));
                lista.add(usuario);

             }
            
            con.getConnection().close();
             
             
            
        } catch (Exception e) {
           logger.warning("error al recuperar lista de usuarios "+e.getMessage());
            return lista;
        }
         logger.info("listarUsuarios ok ");
        return lista;
    }
            
            public Usuario getById(int id){
           Conexion con=new Conexion();
         Usuario usuario=null;
        String query="select * from usuario where id=?";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
              statement.setInt(1, id);
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                usuario=new Usuario();
                usuario.setId(rs.getInt("id"));
                usuario.setRut(rs.getString("rut"));
                usuario.setNombre(rs.getString("nombre"));
                usuario.setApellido(rs.getString("apellido"));
                usuario.setDomicilio(rs.getString("domicilio"));
                usuario.setTelefono(rs.getString("telefono"));
                usuario.setCorreo(rs.getString("correo"));
                usuario.setPassword(rs.getString("password"));
                usuario.setEscuela_id(rs.getInt("escuela_id"));
                usuario.setPerfil_id(rs.getInt("perfil_id"));
                
             }
            
            con.getConnection().close();
             
             
            
        } catch (Exception e) {
           logger.warning("error al recuperar  usuario "+e.getMessage());
            return usuario;
        }
        logger.info("getById ok ");
        return usuario;
    }
        public Usuario getByRut(String rut){
           Conexion con=new Conexion();
         Usuario usuario=null;
        String query="select * from usuario where rut=?";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
              statement.setString(1, rut);
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                usuario=new Usuario();
                usuario.setId(rs.getInt("id"));
                usuario.setRut(rs.getString("rut"));
                usuario.setNombre(rs.getString("nombre"));
                usuario.setApellido(rs.getString("apellido"));
                usuario.setDomicilio(rs.getString("domicilio"));
                usuario.setTelefono(rs.getString("telefono"));
                usuario.setCorreo(rs.getString("correo"));
                usuario.setPassword(rs.getString("password"));
                usuario.setEscuela_id(rs.getInt("escuela_id"));
                usuario.setPerfil_id(rs.getInt("perfil_id"));
                
             }
            
            con.getConnection().close();
             
             
            
        } catch (Exception e) {
           logger.warning("error al recuperar  usuario "+e.getMessage());
            return usuario;
        }
        logger.info("getById ok ");
        return usuario;
    }
        
            
            
   public String create(Usuario usuario) throws SQLException {
         Conexion con=new Conexion();
      String query="INSERT INTO usuario(rut,nombre,apellido,domicilio,telefono,correo,password,escuela_id,perfil_id)"
              + " values (?,?,?,?,?,?,?,?,?)";
        try {
            
            PreparedStatement statement = con.getConnection().prepareStatement(query);
         
            statement.setString(1, usuario.getRut());
            statement.setString(2, usuario.getNombre());
            statement.setString(3, usuario.getApellido());
            statement.setString(4, usuario.getDomicilio());
            statement.setString(5, usuario.getTelefono());
            statement.setString(6, usuario.getCorreo());
            statement.setString(7, usuario.getPassword());
            statement.setInt(8, usuario.getEscuela_id());
            statement.setInt(9, usuario.getPerfil_id());
            
            
            
            statement.executeUpdate();
              
           
            con.getConnection().close();
            
        } catch (SQLException ex) {
            logger.warning("error al crear usuario "+ex.toString()+" "+ex.getMessage());
            return ex.toString()+" "+ex.getMessage();
        }
         logger.warning("usuario creada");
        return "ok";
        
        
    }
                         
    public String delete(int id) throws SQLException {
           Conexion con=new Conexion();
         String query ="DELETE FROM usuario WHERE id = ?";
         
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
            con.getConnection().close();
        } catch (SQLException ex) {
          logger.warning("error al eliminar usuario"+ex.getMessage());
          return ex.getMessage();
        }
         
        logger.warning("usuario eliminada ok");
        return "ok";
    }
    
     public String edit(Usuario usuario) throws SQLException {
        Conexion con=new Conexion();
      
         String query="UPDATE usuario SET"
                + " rut = ?"   
                + " nombre = ?"
                + " apellido = ?" 
                + " domicilio = ?"   
                + " telefono = ?"  
                + " correo = ?"  
                + " password = ?"  
                + " escuela_id = ?"   
                + " perfil_id = ?"    
                + " WHERE id = ? ;";
        
        
       try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setString(1, usuario.getRut()); 
            statement.setString(2, usuario.getNombre());
            statement.setString(3, usuario.getApellido()); 
            statement.setString(4, usuario.getDomicilio()); 
            statement.setString(5, usuario.getTelefono()); 
            statement.setString(6, usuario.getCorreo()); 
            statement.setString(7, usuario.getPassword()); 
            statement.setInt(8, usuario.getEscuela_id());
            statement.setInt(9, usuario.getPerfil_id());
            statement.setInt(10, usuario.getId()); 
            statement.executeUpdate();
            
            con.getConnection().close();
        } catch (SQLException ex) {
          logger.warning("error al acutalizar usuario "+ex.getMessage());
          return ex.getMessage();
        }
     logger.warning("usuario Actualizada ok");
     return "ok";

     
    }
}
