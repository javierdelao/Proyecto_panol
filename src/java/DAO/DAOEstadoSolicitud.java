/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.EstadoSolicitud;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOEstadoSolicitud {
    Logger logger=Logger.getLogger(Conexion.class);
    
    public ArrayList<EstadoSolicitud> listar(){
        Conexion con=new Conexion();
        ArrayList<EstadoSolicitud>lista=new ArrayList<>();
        
        String query="select * from estado_solicitud";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                EstadoSolicitud estadosolicitud = new EstadoSolicitud();
                estadosolicitud.setId(rs.getInt("id"));
                estadosolicitud.setDescripcion(rs.getString("descripcion"));
                
                lista.add(estadosolicitud);
            }
            con.getConnection().close();
            
        } catch (Exception e) {
           logger.warning("Error al recuperar lista de estados de solicitudes. "+e.getMessage());
           return lista;
        }
        logger.info("Listar estados de solicitudes ok.");
        return lista;
    }
    
    public EstadoSolicitud getById(int id){
        Conexion con=new Conexion();
        EstadoSolicitud estadosolicitud=null;
        String query="select * from estado_solicitud where id=?";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                estadosolicitud=new EstadoSolicitud();
                estadosolicitud.setId(rs.getInt("id"));
                estadosolicitud.setDescripcion(rs.getString("descripcion"));
                
            }
            con.getConnection().close();
            
        } catch (Exception e) {
           logger.warning("error al recuperar lista de solicitud "+e.getMessage());
           return estadosolicitud;
        }
        logger.info("edtado solicitud ok ");
        return estadosolicitud;
    }
    
    public String create(EstadoSolicitud estadosolicitud) throws SQLException {
        Conexion con=new Conexion();
        String query="INSERT INTO estado_solicitud(descripcion)"
            + " values (?)";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setString(1, estadosolicitud.getDescripcion());
            statement.executeUpdate();
            
            con.getConnection().close();
            
        } catch (SQLException ex) {
            logger.warning("error al crear estado_solicitud "+ex.toString()+" "+ex.getMessage());
            return ex.toString()+" "+ex.getMessage();
        }
        logger.warning("estado_solicitud creada");
        return "ok";
    }
    
    public String edit(EstadoSolicitud estadosolicitud) throws SQLException {
        Conexion con=new Conexion();
        String query="UPDATE estado_solicitud SET"
                + " descripcion = ?"
                + " WHERE id = ? ;";
        
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setString(1, estadosolicitud.getDescripcion());
            statement.setInt(2, estadosolicitud.getId());
            statement.executeUpdate();
            
            con.getConnection().close();
        } catch (SQLException ex) {
            logger.warning("error al acutalizar estado_solicutd "+ex.getMessage());
            return ex.getMessage();
        }
        logger.warning("estado_solicitud Actualizado ok");
        return "ok";
    }
}
