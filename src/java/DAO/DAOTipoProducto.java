/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.TipoProducto;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOTipoProducto {
      Logger logger=Logger.getLogger(Conexion.class);
      
       public ArrayList<TipoProducto> listar(){
           Conexion con=new Conexion();
          ArrayList<TipoProducto>lista=new ArrayList<>();
        String query="select * from tipo_producto";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
             
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                TipoProducto tipoProd=new TipoProducto();
                tipoProd.setId(rs.getInt("id"));
                tipoProd.setDescripcion(rs.getString("descripcion"));
               
                lista.add(tipoProd);
             }
            con.getConnection().close();  
        } catch (Exception e) {
           logger.warning("error al recuperar lista de TipoProducto "+e.getMessage());
           return lista;
        }
         logger.info("listarTipoProductook ");
        return lista;
    }
       
       
               public TipoProducto getById(int id){
           Conexion con=new Conexion();
         TipoProducto tipoProd=null;
        String query="select * from tipo_producto where id=?";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
              statement.setInt(1, id);
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                tipoProd=new TipoProducto();
                 tipoProd.setId(rs.getInt("id"));
                tipoProd.setDescripcion(rs.getString("descripcion"));
             }
            con.getConnection().close(); 
        } catch (Exception e) {
           logger.warning("error al recuperare TipoProducto "+e.getMessage());
           return tipoProd;
        }
        logger.info("getSedeById TipoProducto ok ");
        return tipoProd;
    }
}
