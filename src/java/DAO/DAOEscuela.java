/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Escuela;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOEscuela {
    Logger logger=Logger.getLogger(Conexion.class);
    
           public ArrayList<Escuela> listar(){
           Conexion con=new Conexion();
          ArrayList<Escuela>lista=new ArrayList<>();
        String query="select * from escuela";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
             
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Escuela escuela=new Escuela();
                escuela.setId(rs.getInt("id"));
                escuela.setNombre(rs.getString("nombre"));
                escuela.setSede_id(rs.getInt("sede_id"));  
                lista.add(escuela);
             }
            
            con.getConnection().close();
             
             
             return lista;
        } catch (Exception e) {
           logger.warning("error al recuperar lista de escuelas "+e.getMessage());
        }
         logger.info("listaEscuelas ok ");
        return lista;
    }
           
           
        public ArrayList<Escuela> listarBySedeId(int id){
           Conexion con=new Conexion();
          ArrayList<Escuela>lista=new ArrayList<>();
        String query="select * from escuela where sede_id=?";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
             statement.setInt(1, id);
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Escuela escuela=new Escuela();
                escuela.setId(rs.getInt("id"));
                escuela.setNombre(rs.getString("nombre"));
                escuela.setSede_id(rs.getInt("sede_id"));  
                lista.add(escuela);
             }
            
            con.getConnection().close();
             
             
             return lista;
        } catch (Exception e) {
           logger.warning("error al recuperar lista de escuelas "+e.getMessage());
        }
         logger.info("listaEscuelas ok ");
        return lista;
    }
           
           
           
           
                   public Escuela getById(int id){
           Conexion con=new Conexion();
         Escuela escuela=null;
        String query="select * from escuela where id=?";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
              statement.setInt(1, id);
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                escuela= new Escuela();
                escuela.setId(rs.getInt("id"));
                escuela.setNombre(rs.getString("nombre"));
                escuela.setSede_id(rs.getInt("sede_id"));
             }
            
            con.getConnection().close();
             
             
             return escuela;
        } catch (Exception e) {
           logger.warning("error al recuperar lista de escuelas "+e.getMessage());
        }
        logger.info("getSedeById escuela ok ");
        return escuela;
    }
}
