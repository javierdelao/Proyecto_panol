/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Subcategoria;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOSubcategoria {
    Logger logger=Logger.getLogger(Conexion.class);
     
    public ArrayList<Subcategoria> listar(){
        Conexion con=new Conexion();
        ArrayList<Subcategoria>lista=new ArrayList<>();
        String query="select * from subcategoria";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Subcategoria subC=new Subcategoria();
                subC.setId(rs.getInt("id"));
                subC.setNombre(rs.getString("nombre"));
                subC.setCategoria_id(rs.getInt("categoria_id"));
                subC.setStock(rs.getInt("stock"));
                subC.setTipo_producto_id(rs.getInt("tipo_producto_id"));
               
                lista.add(subC);
            }
            con.getConnection().close();  
        } catch (Exception e) {
           logger.warning("error al recuperar lista de Subcategoria "+e.getMessage());
           return lista;
        }
        logger.info("listar Subcategoria ok ");
        return lista;
    }
    
    public ArrayList<Subcategoria> listarByCondiciones(String nombre, String categoria){
        Conexion con=new Conexion();
        ArrayList<Subcategoria>lista=new ArrayList<>();
        
        int tipoFiltro = 0;
        int categoriaInt = 0;
        
        String query = "SELECT * FROM subcategoria WHERE";
        if(!nombre.isEmpty() && !categoria.isEmpty()){
            query += " nombre LIKE ? AND categoria_id = ?";
            categoriaInt = Integer.parseInt(categoria);
            
            tipoFiltro = 1;
            
        } else {
        
            if(!nombre.isEmpty()){
                query += " nombre LIKE ?";
                tipoFiltro = 2;
            }
            if(!categoria.isEmpty()){
                query += " categoria_id = ?";
                categoriaInt = Integer.parseInt(categoria);
                
                tipoFiltro = 3;
            }
        }
        try{
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            switch(tipoFiltro){
                case 1:
                    statement.setString(1, "%"+nombre+"%");
                    statement.setInt(2, categoriaInt);
                    break;
                case 2:
                    statement.setString(1,"%"+nombre+"%");
                    break;
                case 3:
                    statement.setInt(1, categoriaInt);
                    break;
            }
            
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Subcategoria subC=new Subcategoria();
                subC.setId(rs.getInt("id"));
                subC.setNombre(rs.getString("nombre"));
                subC.setCategoria_id(rs.getInt("categoria_id"));
                subC.setStock(rs.getInt("stock"));
                subC.setTipo_producto_id(rs.getInt("tipo_producto_id"));
               
                lista.add(subC);
            }
            con.getConnection().close();
            
        } catch(Exception ex){
            logger.warning("Error al recuperar lista de Subcategorias. "+ex.getMessage());
            return lista;
        }
        logger.warning("Subcategorías filtradas. ");
        return lista;
        
    }
           
           
    public Subcategoria getById(int id){
        Conexion con=new Conexion();
        Subcategoria subC=null;
        String query="select * from subcategoria where id=?";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                subC=new Subcategoria();
                subC.setId(rs.getInt("id"));
                subC.setNombre(rs.getString("nombre"));
                subC.setCategoria_id(rs.getInt("categoria_id"));
                subC.setStock(rs.getInt("stock"));
                subC.setTipo_producto_id(rs.getInt("tipo_producto_id"));
            }
            con.getConnection().close(); 
        } catch (Exception e) {
           logger.warning("error al recuperar lista de Subcategoria "+e.getMessage());
           return subC;
        }
        logger.info("getSedeById Subcategoria ok ");
        return subC;
    }
     
    public String create(Subcategoria subC) throws SQLException {
        Conexion con=new Conexion();
        String query="INSERT INTO subcategoria(nombre,categoria_id,stock,tipo_producto_id)"
            + " values (?,?,?,?)";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setString(1, subC.getNombre());
            statement.setInt(2, subC.getCategoria_id());
            statement.setInt(3, subC.getStock());
            statement.setInt(4, subC.getTipo_producto_id());
            statement.executeUpdate();
            con.getConnection().close();
            
        } catch (SQLException ex) {
            logger.warning("error al crear Subcategoria "+ex.toString()+" "+ex.getMessage());
            return ex.toString()+" "+ex.getMessage();
        }
        logger.warning("sede Subcategoria");
        return "ok";
    }
    
    public String edit(Subcategoria subC) throws SQLException {
        Conexion con=new Conexion();

        String query="UPDATE subcategoria SET"
            + " nombre = ?,"
            + " categoria_id = ?,"
            + " stock = ?,"
            + " tipo_producto_id = ?"
            + " WHERE id = ? ;";
        
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setString(1, subC.getNombre());    
            statement.setInt(2, subC.getCategoria_id());
            statement.setInt(3, subC.getStock());
            statement.setInt(4, subC.getTipo_producto_id());
            statement.setInt(5, subC.getId());
            statement.executeUpdate();
            
            con.getConnection().close();
        } catch (SQLException ex) {
          logger.warning("error al acutalizar Subcategoria "+ex.getMessage());
          return ex.getMessage();
        }
        logger.warning("Subcategoria Actualizada ok");
        return "ok";
    }
}
