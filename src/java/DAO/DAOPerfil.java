/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Perfil;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOPerfil {
     Logger logger=Logger.getLogger(Conexion.class);
     
           public ArrayList<Perfil> listar(){
           Conexion con=new Conexion();
          ArrayList<Perfil>lista=new ArrayList<>();
        String query="select * from perfil";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
             
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Perfil perfil=new Perfil();
                perfil.setId(rs.getInt("id"));
                perfil.setDescripcion(rs.getString("descripcion"));
               
                lista.add(perfil);

             }
            
            con.getConnection().close();
             
             
             
        } catch (Exception e) {
           logger.warning("error al recuperar lista de perfiles "+e.getMessage());
           return lista;
        }
         logger.info("listarperfiles ok ");
        return lista;
    }
           
           
    public Perfil getById(int id){
           Conexion con=new Conexion();
         Perfil perfil=null;
        String query="select * from perfil where id=?";
        try {
             PreparedStatement statement = con.getConnection().prepareStatement(query);
              statement.setInt(1, id);
              ResultSet rs = statement.executeQuery();
            while(rs.next()){
                perfil=new Perfil();
                perfil.setId(rs.getInt("id"));
                perfil.setDescripcion(rs.getString("descripcion"));
             }
            
            con.getConnection().close();
             
             
             
        } catch (Exception e) {
           logger.warning("error al recuperar perfil "+e.getMessage());
           return perfil;
        }
        logger.info("getSedeById perfil ok ");
        return perfil;
    }
}
