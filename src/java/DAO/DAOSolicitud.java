/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Solicitud;
import com.sun.istack.internal.logging.Logger;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOSolicitud {
    Logger logger=Logger.getLogger(Conexion.class);
    
    public ArrayList<Solicitud> listar(){
        Conexion con=new Conexion();
        ArrayList<Solicitud>lista=new ArrayList<>();
        String query="select * from solicitud";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Solicitud solicitud=new Solicitud();
                solicitud.setId(rs.getInt("id"));
                solicitud.setUsuario_id(rs.getInt("usuario_id"));
                solicitud.setTipo_solicitud_id(rs.getInt("tipo_solicitud_id"));
                solicitud.setEstado_solicitud_id(rs.getInt("estado_solicitud_id"));
                solicitud.setObservacion(rs.getString("observacion"));
                solicitud.setReceptor_id(rs.getInt("receptor_id"));
                solicitud.setFechaCreacion(rs.getDate("fecha_creacion"));
                
                lista.add(solicitud);
            }
            con.getConnection().close();
            
        } catch (Exception e) {
            logger.warning("error al recuperar lista de solicitud "+e.getMessage());
            return lista;
        }
        logger.info("listar solicitud ok ");
        return lista;
    }
    
    public ArrayList<Solicitud> listarSolicitudesByUsuario(int usuario_id, boolean conRelacionProductos, boolean conProductos){
        Conexion con=new Conexion();
        ArrayList<Solicitud>lista=new ArrayList<>();
        String query="SELECT * FROM solicitud WHERE usuario_id = ? AND tipo_solicitud_id = 1 AND estado_solicitud_id IN (1,2,6) ORDER BY id DESC;";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, usuario_id);
            ResultSet rs = statement.executeQuery();
            
            while(rs.next()){
                Solicitud solicitud=new Solicitud();
                solicitud.setId(rs.getInt("id"));
                solicitud.setUsuario_id(rs.getInt("usuario_id"));
                solicitud.setTipo_solicitud_id(rs.getInt("tipo_solicitud_id"));
                solicitud.setEstado_solicitud_id(rs.getInt("estado_solicitud_id"));
                solicitud.setObservacion(rs.getString("observacion"));
                solicitud.setReceptor_id(rs.getInt("receptor_id"));
                solicitud.setFechaCreacion(rs.getDate("fecha_creacion"));
                
                if(conRelacionProductos){
                    DAOSolicitudProductos daoSolicitudProds = new DAOSolicitudProductos();
                    
                    solicitud.setListadoSubcategorias(
                        daoSolicitudProds.listarBySolicitud(solicitud.getId(),conProductos)
                    );
                }
                
                lista.add(solicitud);
            }
            con.getConnection().close();
            
        } catch (Exception e) {
            logger.warning("error al recuperar lista de solicitud. "+e.getMessage());
            return lista;
        }
        logger.info("listar solicitud ok ");
        return lista;
    }
    
    public ArrayList<Solicitud> listarSolicitudesEnviadas(boolean conUsuario){
        Conexion con = new Conexion();
        ArrayList<Solicitud>lista=new ArrayList<>();
        String query="SELECT * FROM solicitud WHERE estado_solicitud_id IN (2,6) ORDER BY id DESC";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Solicitud solicitud=new Solicitud();
                solicitud.setId(rs.getInt("id"));
                solicitud.setUsuario_id(rs.getInt("usuario_id"));
                solicitud.setTipo_solicitud_id(rs.getInt("tipo_solicitud_id"));
                solicitud.setEstado_solicitud_id(rs.getInt("estado_solicitud_id"));
                solicitud.setObservacion(rs.getString("observacion"));
                solicitud.setReceptor_id(rs.getInt("receptor_id"));
                solicitud.setFechaCreacion(rs.getDate("fecha_creacion"));
                
                if(conUsuario){
                    DAOUsuario daoUsuario = new DAOUsuario();
                    solicitud.setUsuario( daoUsuario.getById(solicitud.getUsuario_id()) );
                }
                
                lista.add(solicitud);
            }
            con.getConnection().close();
            
        } catch (Exception e) {
            logger.warning("Error al consultar solicitudes no en curso. "+e.getMessage());
            return lista;
        }
        logger.info("Listar solicitudes no en curso ok.");
        return lista;
    }
    
    public ArrayList<Solicitud> getAllPrestamos(boolean conUsuario, int usuarioId, int estadoSolicitudId, boolean conRelacionProductos){
        Conexion con = new Conexion();
        String query = "SELECT * FROM solicitud WHERE";
        
        if(estadoSolicitudId > 0 && usuarioId > 0){
            query += " estado_solicitud_id = "+estadoSolicitudId;
            query += " AND usuario_id = "+usuarioId;
        } else {
            if(estadoSolicitudId > 0){
                query += " estado_solicitud_id = "+estadoSolicitudId;
            } else {
                query += " estado_solicitud_id IN (3,4,5)";
            }

            if(usuarioId > 0){
                query += " AND usuario_id = '"+usuarioId+"'";
            }
        }
        query += " ORDER BY id DESC";
        
        ArrayList<Solicitud> prestamos = new ArrayList<>();
        
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Solicitud solicitud=new Solicitud();
                solicitud.setId(rs.getInt("id"));
                solicitud.setUsuario_id(rs.getInt("usuario_id"));
                solicitud.setTipo_solicitud_id(rs.getInt("tipo_solicitud_id"));
                solicitud.setEstado_solicitud_id(rs.getInt("estado_solicitud_id"));
                solicitud.setObservacion(rs.getString("observacion"));
                solicitud.setReceptor_id(rs.getInt("receptor_id"));
                solicitud.setFechaCreacion(rs.getDate("fecha_creacion"));
                
                if(conUsuario){
                    DAOUsuario daoUsuario = new DAOUsuario();
                    solicitud.setUsuario( daoUsuario.getById(solicitud.getUsuario_id()) );
                }
                
                if(conRelacionProductos){
                    DAOSolicitudProductos daoSolicitudProds = new DAOSolicitudProductos();
                    
                    solicitud.setListadoSubcategorias(
                        daoSolicitudProds.listarBySolicitud(solicitud.getId(),true)
                    );
                }
                
                prestamos.add(solicitud);
            }
            con.getConnection().close();
            
        } catch (Exception e) {
            logger.warning("Error al consultar los préstamos. "+e.getMessage());
            return prestamos;
        }
        logger.warning("Prestamos recuperados.");
        return prestamos;
    }
    
    public Solicitud getById(int id, boolean conRelacionProductos, boolean conProductos){
        Conexion con=new Conexion();
        Solicitud solicitud=null;
        String query="select * from solicitud where id=?";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                solicitud=new Solicitud();
                solicitud.setId(rs.getInt("id"));
                solicitud.setUsuario_id(rs.getInt("usuario_id"));
                solicitud.setTipo_solicitud_id(rs.getInt("tipo_solicitud_id"));
                solicitud.setEstado_solicitud_id(rs.getInt("estado_solicitud_id"));
                solicitud.setObservacion(rs.getString("observacion"));
                solicitud.setReceptor_id(rs.getInt("receptor_id"));
                solicitud.setFechaCreacion(rs.getDate("fecha_creacion"));
                
                if(conRelacionProductos){
                    DAOSolicitudProductos daoSolicitudProds = new DAOSolicitudProductos();
                    
                    solicitud.setListadoSubcategorias(
                        daoSolicitudProds.listarBySolicitud(solicitud.getId(),conProductos)
                    );
                }
            }
            con.getConnection().close();
            
        } catch (Exception e) {
           logger.warning("Error al recuperar solicitudById. "+e.getMessage());
           return solicitud;
        }
        logger.info("getSolicitudById solicitud ok ");
        return solicitud;
    }
    
    public Solicitud getSolicitudActiva(int usuario_id){
        Conexion con=new Conexion();
        Solicitud solicitud=null;
        String query = "SELECT * FROM solicitud WHERE usuario_id = ? AND estado_solicitud_id = 1";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, usuario_id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                solicitud=new Solicitud();
                solicitud.setId(rs.getInt("id"));
                solicitud.setUsuario_id(rs.getInt("usuario_id"));
                solicitud.setTipo_solicitud_id(rs.getInt("tipo_solicitud_id"));
                solicitud.setEstado_solicitud_id(rs.getInt("estado_solicitud_id"));
                solicitud.setObservacion(rs.getString("observacion"));
                solicitud.setReceptor_id(rs.getInt("receptor_id"));
                solicitud.setFechaCreacion(rs.getDate("fecha_creacion"));
            }
            con.getConnection().close();
            
        } catch (Exception e) {
           logger.warning("error al recuperar solicitud "+e.getMessage());
           return solicitud;
        }
        logger.info("getSolicitudActiva solicitud ok ");
        return solicitud;
    }
    
    public String create(Solicitud solicitud) throws SQLException {
        Conexion con=new Conexion();
        String query="INSERT INTO solicitud(usuario_id,tipo_solicitud_id,estado_solicitud_id,observacion,receptor_id,fecha_creacion,fecha_prestamo)"
            + " values (?,?,?,?,?,?,?)";
        try {
            
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, solicitud.getUsuario_id());
            statement.setInt(2, solicitud.getTipo_solicitud_id());
            statement.setInt(3, solicitud.getEstado_solicitud_id());
            statement.setString(4, solicitud.getObservacion());
            statement.setInt(5, solicitud.getReceptor_id());
            
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String fechaFormateada = dateFormat.format(solicitud.getFecha_creacion());
            statement.setDate(6, Date.valueOf(fechaFormateada));
            
            
            String fechaPrestamoFormateada = dateFormat.format(solicitud.getFecha_prestamo());
            statement.setDate(7, Date.valueOf(fechaPrestamoFormateada));
            
            statement.executeUpdate();
            
            con.getConnection().close();
            
        } catch (SQLException ex) {
            logger.warning("error al crear solicitud "+ex.toString()+" "+ex.getMessage());
            return ex.toString()+" "+ex.getMessage();
        }
        logger.warning("solicitud creada");
        return "ok";
    }
    
    public String edit(Solicitud solicitud) throws SQLException {
        Conexion con=new Conexion();
        String query="UPDATE solicitud SET"
                + " usuario_id = ?,"
                + " tipo_solicitud_id = ?,"
                + " estado_solicitud_id = ?,"
                + " observacion = ?,"
                + " receptor_id = ?"
                + " WHERE id = ? ;";
        
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, solicitud.getUsuario_id());    
            statement.setInt(2, solicitud.getTipo_solicitud_id()); 
            statement.setInt(3, solicitud.getEstado_solicitud_id());
            statement.setString(4, solicitud.getObservacion());
            statement.setInt(5, solicitud.getReceptor_id());
            statement.setInt(6, solicitud.getId());
            statement.executeUpdate();
            
            con.getConnection().close();
        } catch (SQLException ex) {
          logger.warning("error al actualizar solicitud "+ex.getMessage());
          return ex.getMessage();
        }
        logger.warning("solicitud Actualizada ok");
        return "ok";
    }
}
