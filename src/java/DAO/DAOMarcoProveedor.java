/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.MarcaProveedor;
import com.sun.istack.internal.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class DAOMarcoProveedor {
    Logger logger=Logger.getLogger(Conexion.class);
    
    public ArrayList<MarcaProveedor> listar(){
        Conexion con=new Conexion();
        ArrayList<MarcaProveedor>lista=new ArrayList<>();
        String query="select * from marca_proveedor";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                MarcaProveedor marcaProv=new MarcaProveedor();
                marcaProv.setId(rs.getInt("id"));
                marcaProv.setNombre(rs.getString("nombre"));
               
                lista.add(marcaProv);
            }
            con.getConnection().close();  
        } catch (Exception e) {
           logger.warning("error al recuperar lista de MarcaProveedor "+e.getMessage());
           return lista;
        }
        logger.info("listarMarcaProveedor ok ");
        return lista;
    }
       
       
    public MarcaProveedor getById(int id){
        Conexion con=new Conexion();
        MarcaProveedor marcaProv=null;
        String query="select * from marca_proveedor where id=?";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                marcaProv=new MarcaProveedor();
                marcaProv.setId(rs.getInt("id"));
                marcaProv.setNombre(rs.getString("nombre"));
                
            }
            con.getConnection().close(); 
        } catch (Exception e) {
           logger.warning("error al recuperare MarcaProveedor "+e.getMessage());
           return marcaProv;
        }
        logger.info("getSedeById MarcaProveedor ok ");
        return marcaProv;
    }
    
    public String create(MarcaProveedor marca) throws SQLException {
        Conexion con=new Conexion();
        String query = "INSERT INTO marca_proveedor(nombre)" + " values (?)";
        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setString(1, marca.getNombre());
            statement.executeUpdate();
            con.getConnection().close();
            
        } catch (SQLException ex) {
            logger.warning("error al crear Marca "+ex.toString()+" "+ex.getMessage());
            return ex.toString()+" "+ex.getMessage();
        }
        logger.warning("Marca creada");
        return "ok";
    }
    
    public String edit(MarcaProveedor marca) throws SQLException {
        Conexion con=new Conexion();

        String query="UPDATE marca_proveedor SET"
            + " nombre = ?"
            + " WHERE id = ? ;";

        try {
            PreparedStatement statement = con.getConnection().prepareStatement(query);
            statement.setString(1, marca.getNombre());    
            statement.setInt(2, marca.getId()); 
            statement.executeUpdate();
            
            con.getConnection().close();
        } catch (SQLException ex) {
            logger.warning("error al acutalizar marca "+ex.getMessage());
            return ex.getMessage();
        }
        logger.warning("marca Actualizada ok");
        return "ok";
    }
}
